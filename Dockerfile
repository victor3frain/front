FROM node:16.14.2-alpine3.15

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

RUN apk update && apk add bash
#RUN npm --version

COPY package.json ./
COPY package-lock.json ./

RUN npm cache clean -f
RUN npm install npm@^8 -g

RUN npm install 
RUN npm install -g npm@latest

#RUN nvm install
RUN npm i --save @microsoft/signalr

RUN npm install ce
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent
RUN npm i react-scripts 
RUN npm install --save react react-dom react-scripts
#RUN npm install --save-dev extract-text-webpack-plugin
RUN npm i fullcalendar --silent
RUN npm install @mui/material --silent
RUN npm install @mui/lab --silent
RUN npm install @mui/x-data-grid --silent

RUN npm install --save-dev webpack

#RUN npm remove webpack
#RUN npm install webpack@3.11.0
RUN npm install webpack@5.72.1
#RUN npm cache clean --force
RUN npm i
RUN npm -v
RUN ls -l
RUN pwd
#RUN npm run build
RUN mkdir -p node_modules/.cache && chmod -R 777 node_modules/.cache


COPY . ./

RUN ls -l 

CMD ["npm", "start"]
