

const path = require('path');

module.exports = {
    output: {
        path: path.resolve(__dirname, 'public_html/js'),
        filename: 'app.js'
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$|jsx/,
            exclude: /node_modules/
        },
        {
            test: /\.(js|jsx)$/,
            include: path.appSrc,
            loader: require.resolve('babel-loader'),
            options: {
                // This is a feature of `babel-loader` for Webpack (not Babel itself).
                // It enables caching results in ./node_modules/.cache/babel-loader/
                // directory for faster rebuilds.
                cacheDirectory: true,
                plugins: ['react-hot-loader/babel'],
            },
        }]
    },
    entry: {
        main: './src/index.js'
    }
}