import React, { Component } from 'react';
import { Switch, Route } from 'react-router'
import Login from './components/Login/Login.js';
import Forgot_Pass from './components/Login/Forgot_Pass.js';
import NewAccount from './components/Login/NewAccount.js';
import Recover_Pass from './components/Login/Recover_Pass.js';
import Page404 from './components/Error/Page404.js';
import Assigments from './components/DevOps/Assigments.js';
import ProdDeploy from './components/ProdDeploy/ProdDeploy.js';
import Home from './components/Home/Home.js';
import Planets from './components/DevOps/Planets.js';
import DeployNotes from './components/ProdDeploy/Deploy_Notes/DeployNotes.js';
import Architects from './components/DevOps/Architects.js';
import DevelopmentApi from './components/Developement/API.js';
import Autotemplates from './components/DevOps/Autotemplate.js';

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/home" component={Home} />
          <Route path="/forgotpass" component={Forgot_Pass} />
          <Route path="/newaccount" component={NewAccount} />
          <Route path="/recoverpass" component={Recover_Pass} />
          <Route path="/devops_assigns" component={Assigments} />
          <Route path="/prod_deploy_date" component={ProdDeploy} />
          <Route path="/planetas" component={Planets} />
          <Route path="/deploy_notes" component={DeployNotes} />
          <Route path="/architects" component={Architects} />
          <Route path="/development_api" component={DevelopmentApi}/>
          <Route path="/autotemplate" component={Autotemplates} />
          <Route path="*" component={Page404} />
        </Switch>
      </div>
    )
  };
}