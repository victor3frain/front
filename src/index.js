import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom';
import App from './App';

const rootElement = document.getElementById("root");
//const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');

ReactDOM.render(
  <BrowserRouter basename="/">
    <App />
  </BrowserRouter>,
  rootElement);

if (module.hot) {

  module.hot.accept('./App', () => {

    const NextApp = require('./App').default;

    ReactDOM.render(
      <BrowserRouter basename="/">
        <NextApp />
      </BrowserRouter>,
      document.getElementById('root')
    );

  });
}