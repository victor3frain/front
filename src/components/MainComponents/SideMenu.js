import React, { useEffect }/*, { useState }*/ from 'react';
import { Link } from 'react-router-dom';

function SideMenu(props) {
    var GitLabRequests = 2;
    var JenkinsRequests = 5;
    var ApigeeRequests = 8;

    useEffect(() => {
        const trees = window.$('[data-widget="treeview"]');
        trees.Treeview('init');
    }, []);

    return (
        <aside className="main-sidebar sidebar-dark-warning elevation-4">
            {/* Brand Logo */}
            <a href="/home" className="brand-link">
                <img src="media/logos/DevSecOps.png" alt="Coe Ticket" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
                <span className="brand-text font-weight-light">Coe Ticket</span>
            </a>
            {/* Sidebar */}
            <div className="sidebar">
                {/* Sidebar user panel (optional) */}
                <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="image">
                        <img src="media/users/user_logo.png" className="img-circle elevation-2" alt="User " />
                    </div>
                    <div className="info">
                        <a href="/user" className="d-block">{props.user.name}</a>
                    </div>
                </div>
                {/* SidebarSearch Form */}
                <div className="form-inline">
                    <div className="input-group" data-widget="sidebar-search">
                        <input className="form-control form-control-sidebar" type="search" placeholder="Buscar" aria-label="Search" />
                        <div className="input-group-append">
                            <button className="btn btn-sidebar">
                                <i className="fas fa-search fa-fw" />
                            </button>
                        </div>
                    </div>
                </div>
                {/* Sidebar Menu */}
                <nav className="mt-2">
                    <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
                        <li className="nav-item">
                            <Link to="pages/calendar.html" className="nav-link">
                                <i className="nav-icon fab fa-gitlab"></i>
                                <p>
                                    GitLab
                                    <span className={"badge badge-" + (GitLabRequests <= 2 ? "info" : GitLabRequests <= 5 ? "warning" : "danger") + " right"}>{GitLabRequests}</span>
                                </p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="pages/calendar.html" className="nav-link">
                                <i className="nav-icon fab fa-jenkins"></i>
                                <p>
                                    Jenkins
                                    <span className={"badge badge-" + (JenkinsRequests <= 2 ? "info" : JenkinsRequests <= 5 ? "warning" : "danger") + " right"}>{JenkinsRequests}</span>
                                </p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="pages/calendar.html" className="nav-link">
                                <i className="nav-icon fab fa-google"></i>
                                <p>
                                    Apigee
                                    <span className={"badge badge-" + (ApigeeRequests <= 2 ? "info" : ApigeeRequests <= 5 ? "warning" : "danger") + " right"}>{ApigeeRequests}</span>
                                </p>
                            </Link>
                        </li>
                        {
                            (props.user.role && props.user.role.toUpperCase()) === 'DEVOPS' ?
                                <li className="nav-item">
                                    <a href="/" className="nav-link active">
                                        <i className="nav-icon fab fa-dev" />
                                        <p>
                                            DevOps
                                            <i className="right fas fa-angle-left" />
                                        </p>
                                    </a>
                                    <ul className="nav nav-treeview">
                                        <li className="nav-item">
                                            <Link to="/devops_asigns" className="nav-link">
                                                <i className="fas fa-tasks" />
                                                <p className='m-2'>Asignaciones</p>
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to="/planetas" className="nav-link">
                                                <i className="fas fa-globe" />
                                                <p className='m-2'>Planetas</p>
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to="/architects" className="nav-link">
                                                <i className="fa fa-users" />
                                                <p className='m-2'>Lista de arquitectos</p>
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to="/autotemplate" className="nav-link">
                                                <i className="fa fa-file-invoice" />
                                                <p className='m-2'>Autotemplates</p>
                                            </Link>
                                        </li>
                                    </ul>
                                </li>
                                : <div />
                        }

                        <li className="nav-item">
                            <a href="/" className="nav-link active">
                                <i className="nav-icon fas fa-code" />
                                <p>
                                    Desarrollo
                                    <i className="right fas fa-angle-left" />
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                    <Link to="/development_api" className="nav-link">
                                        <i className="fas fa-globe" />
                                        <p className='m-2'>APIs</p>
                                    </Link>
                                </li>
                            </ul>
                        </li>

                        <li className="nav-item">
                            <a href="/" className="nav-link active">
                                <i className="nav-icon fas fa-plane-departure" />
                                <p>
                                    Despliegues
                                    <i className="right fas fa-angle-left" />
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                    <Link to="/prod_deploy_date" className="nav-link">
                                        <i className="fas fa-calendar-week" />
                                        <p className='m-2'>Calendario</p>
                                    </Link>
                                </li>
                                {(props.user.role && props.user.role.toUpperCase()) === 'DEVOPS' ?
                                    <li className="nav-item">
                                        <Link to="/deploy_notes" className="nav-link">
                                            <i className="fas fa-sticky-note" />
                                            <p className='m-2'>Notas</p>
                                        </Link>
                                    </li>
                                    : []
                                }
                            </ul>
                        </li>
                    </ul>
                </nav>
                {/* /.sidebar-menu */}
            </div>
            {/* /.sidebar */}
        </aside>
    )
}
export default SideMenu;