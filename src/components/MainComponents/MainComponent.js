import React from 'react';
import NavMenu from './NavMenu.js';
import SideMenu from './SideMenu.js';
import Content from './Content.js';
import Footer from './Footer.js';

function MainComponent(props) {
    return (
        <div>
            <NavMenu user={props.User} checkingSessionId={props.SessionInterval} />
            <SideMenu user={props.User} checkingSessionId={props.SessionInterval} />
            <Content user={props.User} checkingSessionId={props.SessionInterval}
                Title={props.Title}
                Route={props.Route}
                component={props.Component} />
            <Footer user={props.User} checkingSessionId={props.SessionInterval} />
        </div>
    )
} export default MainComponent;