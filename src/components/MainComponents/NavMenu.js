import React from 'react';
import { useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';
import Utils from '../Utils/Utils.js';
import Settings from '../Utils/configsettings.js';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';


function NavMenu(props) {

    let History = useHistory();

    const HtmlTooltip = styled(({ className, ...props }) => (
        <Tooltip {...props} classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: '#f5f5f9',
            color: 'rgba(0, 0, 0, 0.87)',
            maxWidth: 220,
            fontSize: theme.typography.pxToRem(12),
            border: '1px solid #dadde9',
        },
    }));

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    function LogOut(event) {
        event.preventDefault();
        const user = Utils.getCookie("user");
        axios({
            method: 'put',
            url: Settings.GetApiIp()+"/CoeTicketSec/login/logout",
            data: { data: user },
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                })
            }
            else {
                if (typeof response.data.body !== 'object')
                    data = JSON.parse(response.data.body);

                if (data.body.success) {
                    Utils.setCookie("user", null, -360);
                    clearInterval(props.SessionInterval);
                    History.push("/")
                }
                else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Ocurrió un problema al cerrar sesión'
                    })
                }
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrio un error inesperado'
            })
        });
    }

    return (
        <nav className="main-header navbar navbar-expand navbar-white navbar-light">
            {/* Left navbar links */}
            <ul className="navbar-nav">
                <li className="nav-item">
                    <a className="nav-link" data-widget="pushmenu" href="/" role="button"><i className="fas fa-bars" /></a>
                </li>
                <li className="nav-item d-none d-sm-inline-block">
                    <Link to="/home" className="nav-link">Inicio</Link>
                </li>
            </ul>
            {/* Right navbar links */}
            <ul className="navbar-nav ml-auto">
                {/* Navbar Search */}
                <HtmlTooltip
                    title={
                        <React.Fragment>
                            <Typography color="inherit">Buscar</Typography>
                            Buscar dentro de la página
                        </React.Fragment>
                    }
                >
                    <li className="nav-item">
                        <a className="nav-link" data-widget="navbar-search" href="/" role="button">
                            <i className="fas fa-search" />
                        </a>
                        <div className="navbar-search-block">
                            <form className="form-inline">
                                <div className="input-group input-group-sm">
                                    <input className="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                                    <div className="input-group-append">
                                        <button className="btn btn-navbar" type="submit">
                                            <i className="fas fa-search" />
                                        </button>
                                        <button className="btn btn-navbar" type="button" data-widget="navbar-search">
                                            <i className="fas fa-times" />
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                </HtmlTooltip>

                {/* Messages Dropdown Menu */}
                <li className="nav-item dropdown">
                    <a className="nav-link" data-toggle="dropdown" href="/">
                        <i className="far fa-comments" />
                        <span className="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="/" className="dropdown-item">
                            {/* Message Start */}
                            <div className="media">
                                <img src="dist/img/user1-128x128.jpg" alt="User Avatar" className="img-size-50 mr-3 img-circle" />
                                <div className="media-body">
                                    <h3 className="dropdown-item-title">
                                        Brad Diesel
                                        <span className="float-right text-sm text-danger"><i className="fas fa-star" /></span>
                                    </h3>
                                    <p className="text-sm">Call me whenever you can...</p>
                                    <p className="text-sm text-muted"><i className="far fa-clock mr-1" /> 4 Hours Ago</p>
                                </div>
                            </div>
                            {/* Message End */}
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item">
                            {/* Message Start */}
                            <div className="media">
                                <img src="dist/img/user8-128x128.jpg" alt="User Avatar" className="img-size-50 img-circle mr-3" />
                                <div className="media-body">
                                    <h3 className="dropdown-item-title">
                                        John Pierce
                                        <span className="float-right text-sm text-muted"><i className="fas fa-star" /></span>
                                    </h3>
                                    <p className="text-sm">I got your message bro</p>
                                    <p className="text-sm text-muted"><i className="far fa-clock mr-1" /> 4 Hours Ago</p>
                                </div>
                            </div>
                            {/* Message End */}
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item">
                            {/* Message Start */}
                            <div className="media">
                                <img src="dist/img/user3-128x128.jpg" alt="User Avatar" className="img-size-50 img-circle mr-3" />
                                <div className="media-body">
                                    <h3 className="dropdown-item-title">
                                        Nora Silvester
                                        <span className="float-right text-sm text-warning"><i className="fas fa-star" /></span>
                                    </h3>
                                    <p className="text-sm">The subject goes here</p>
                                    <p className="text-sm text-muted"><i className="far fa-clock mr-1" /> 4 Hours Ago</p>
                                </div>
                            </div>
                            {/* Message End */}
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link" data-toggle="dropdown" href="/">
                        <i className="far fa-bell" />
                        <span className="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span className="dropdown-item dropdown-header">15 Notifications</span>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item">
                            <i className="fas fa-envelope mr-2" /> 4 new messages
                            <span className="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item">
                            <i className="fas fa-users mr-2" /> 8 friend requests
                            <span className="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item">
                            <i className="fas fa-file mr-2" /> 3 new reports
                            <span className="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div className="dropdown-divider" />
                        <a href="/" className="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>

                {/* Notifications FullScreen*/}
                <HtmlTooltip
                    title={
                        <React.Fragment>
                            <Typography color="inherit">Pantalla completa</Typography>
                        </React.Fragment>
                    }
                >
                    <li className="nav-item">
                        <a className="nav-link" data-widget="fullscreen" href="/" role="button">
                            <i className="fas fa-expand-arrows-alt" />
                        </a>
                    </li>
                </HtmlTooltip>

                {/*Logout*/}
                <HtmlTooltip
                    title={
                        <React.Fragment>
                            <Typography color="inherit">Cerrar sesión</Typography>
                        </React.Fragment>
                    }
                >
                    <li className="nav-item">
                        <a className="nav-link" data-widget="button" href="/" role="button" onClick={e => LogOut(e)}>
                            <i className="fas fa-sign-out-alt" />
                        </a>
                    </li>
                </HtmlTooltip>
            </ul>
        </nav>
    );
}
export default NavMenu;