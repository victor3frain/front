import React from 'react';
import { Link } from 'react-router-dom';

function Content(props) {

    return (
        <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1 className="m-0">{props.Title}</h1>
                        </div>{/* /.col */}
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-sm-right">
                                {props.Route ?
                                    props.Route.map(function (routeObject, index) {
                                        return (<li key={"list-route-" + index} className={'breadcrumb-item ' + index === props.Route.length - 1 ? "active" : ""}>
                                            {index < props.Route.length - 1 ? <Link to={routeObject.Route}>{routeObject.Title}&nbsp;/&nbsp;</Link> : routeObject.Title}
                                            </li>);
                                    }) : {}}

                            </ol>
                        </div>{/* /.col */}
                    </div>{/* /.row */}
                </div>{/* /.container-fluid */}
            </div>
            {/* /.content-header */}
            {/* Main content */}
            <div className="content">
                <div className="container-fluid">
                    {props.component}
                </div>
                {/* /.container-fluid */}
            </div>
            {/* /.content */}
        </div>
    )
}
export default Content;