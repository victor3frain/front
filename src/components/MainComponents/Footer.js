import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {
    return (
        <footer className="main-footer">
            <strong>Copyright &copy; 2022 <Link to="/">COE Ticket</Link>. </strong>
            By DevOps
            Autor: Eduardo Roldán
            Co-Autores: Hector Rubalcava, Victor Pérez y Luis Valdez
            <div className="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.13.0
            </div>
        </footer>
    )
}
export default Footer;