import React from 'react';

function PlanetsComponent(props) {
    return (
        <div className="limiter">
            <div className="container-table100">
                <div className="wrap-table100">
                    <div className="table100 ver5 m-b-110">
                        <table data-vertable="ver5">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>HERRAMIENTAS DEVSECOPS</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">NOMBRE</th>
                                    <th className="column100 column2" data-column="column2">IP</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.63.32.247/jenkins/login">Jenkins</a></td>
                                    <td className="column100 column2" data-column="column2">10.63.32.247</td>
                                    <td className="column100 column3" data-column="column3">22, 8443</td>
                                    <td className="column100 column4" data-column="column4">ssh b266882@10.63.32.247</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0} align="center">
                                        <input name="password" defaultValue="Gu$t4v0$pwd" id="password" />
                                    </td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.63.32.231/users/sign_in">GitLab</a></td>
                                    <td className="column100 column2" data-column="column2">10.63.32.231</td>
                                    <td className="column100 column3" data-column="column3">22, 443</td>
                                    <td className="column100 column4" data-column="column4">ssh b266882@10.63.32.231</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="http://10.63.32.232:8081/artifactory/webapp/#/login">JFrog Artifactory</a></td>
                                    <td className="column100 column2" data-column="column2">10.63.32.232</td>
                                    <td className="column100 column3" data-column="column3">22, 8081</td>
                                    <td className="column100 column4" data-column="column4">ssh b266882@10.63.32.232</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="table100 ver4 m-b-110">
                        <table data-vertable="ver4">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>APIGEE Dev Onpremise (DS1)</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">IP's</th>
                                    <th className="column100 column2" data-column="column2">NODO</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.95</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.95</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0}><input name="password32" defaultValue="G0b13rn04p1s" id="password32" /></td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.96</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.96</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.97</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.97</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.84</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.84</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.85</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.85</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.86</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.86</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.87</td>
                                    <td className="column100 column2" data-column="column2">QPID CLIENT, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.87</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.88</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES CLIENT, POSTGRES SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.88</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.82.56.89:9443/login">10.82.56.89</a></td>
                                    <td className="column100 column2" data-column="column2">OPEN LDAP, MS, UI</td>
                                    <td className="column100 column3" data-column="column3">22, 8443, 9443</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.89</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.90</td>
                                    <td className="column100 column2" data-column="column2">MARKETPLACE</td>
                                    <td className="column100 column3" data-column="column3">22, 8079</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.90</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.98</td>
                                    <td className="column100 column2" data-column="column2">MARKETPLACE</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.90</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="table100 ver4 m-b-110">
                        <table data-vertable="ver4">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>APIGEE Dev Onpremise (DS2)</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">IP's</th>
                                    <th className="column100 column2" data-column="column2">NODO</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                {/* Cambiendo información de Desarrollo*/}
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.79</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.79</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0}><input name="password32" defaultValue="G0b13rn04p1s" id="password32" /></td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.80</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.80</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.154</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.154</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.155</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MESSAGE PROCESOR</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.155</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.156</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MESSAGE PROCESOR</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.156</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.157</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MESSAGE PROCESOR</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.157</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.158</td>
                                    <td className="column100 column2" data-column="column2">QIPID</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.158</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.159</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES SERVER, POSTGRES</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.159</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.56.160</td>
                                    <td className="column100 column2" data-column="column2">MANAGE SERVER </td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.56.160</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="table100 ver1 m-b-110">
                        <table data-vertable="ver1">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>APIGEE QA Onpremise</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">IP's</th>
                                    <th className="column100 column2" data-column="column2">NODO</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.10</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.10</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0}>
                                        <input name="password" defaultValue="G0b13rn04p1s" id="password1" />
                                    </td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.11</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.11</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.12</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.12</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.13</td>
                                    <td className="column100 column2" data-column="column2">MARKETPLACE</td>
                                    <td className="column100 column3" data-column="column3">22, 8079</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.13</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.131</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.131</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.132</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.132</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.133</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.133</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.134</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES CLIENT, POSTGRES SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.134</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.55.135</td>
                                    <td className="column100 column2" data-column="column2">QPID CLIENT, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.55.135</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.82.54.107</td>
                                    <td className="column100 column2" data-column="column2">QPID CLIENT, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.54.107</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.82.54.108:9443/login">10.82.54.108</a></td>
                                    <td className="column100 column2" data-column="column2">OPEN LDAP, MS, UI</td>
                                    <td className="column100 column3" data-column="column3">9443, 22, 8443</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.82.54.108</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.85.8.11</td>
                                    <td className="column100 column2" data-column="column2">Router DMZ</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.85.8.11</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.85.8.12</td>
                                    <td className="column100 column2" data-column="column2">Router DMZ</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.85.8.12</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.85.8.13</td>
                                    <td className="column100 column2" data-column="column2">Router DMZ</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh apigee-admin@10.85.8.13</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="table100 ver2 m-b-110">
                        <table data-vertable="ver2">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>APIGEE DEV AWS</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">IP's</th>
                                    <th className="column100 column2" data-column="column2">NODO</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.11</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER, ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.11</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0}>
                                        <input name="password" defaultValue="G0b13rn04p1sD3v" id="password" />
                                    </td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.12</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER, ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.12</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.13</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.13</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.14</td>
                                    <td className="column100 column2" data-column="column2">QPID CLIENT, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.14</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.15</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES CLIENT, POSTGRES SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.15</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.96.73.16:9443/login">10.96.73.16</a></td>
                                    <td className="column100 column2" data-column="column2">MANAGEMENT SERVER, OPEN LDAP, UI</td>
                                    <td className="column100 column3" data-column="column3">22, 8443, 9443</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.16</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.73.17</td>
                                    <td className="column100 column2" data-column="column2" />
                                    <td className="column100 column3" data-column="column3">22, 8079</td>
                                    <td className="column100 column4" data-column="column4">ssh -i APIGEE-DEV2-KPR.pem ec2-user@10.96.73.17</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="table100 ver3 m-b-110">
                        <table data-vertable="ver3">
                            <thead>
                                <tr>
                                    <th className="column100 column1" data-column="column1" colSpan={5}>APIGEE QA AWS</th>
                                </tr>
                                <tr className="row100 head">
                                    <th className="column100 column1" data-column="column1">IP's</th>
                                    <th className="column100 column2" data-column="column2">NODO</th>
                                    <th className="column100 column3" data-column="column3">PUERTO</th>
                                    <th className="column100 column4" data-column="column4">SSH</th>
                                    <th className="column100 column5" data-column="column5">PASSWORD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.24</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.24</td>
                                    <td className="column100 column5" data-column="column5" rowSpan={0}>
                                        <input name="password" defaultValue="G0b13rn04p1sQA" id="password" />
                                    </td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.25</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.25</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.26</td>
                                    <td className="column100 column2" data-column="column2">CASSANDRA, ZOOKEEPER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.26</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.27</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.27</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.28</td>
                                    <td className="column100 column2" data-column="column2">ROUTER, MP</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.28</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.30</td>
                                    <td className="column100 column2" data-column="column2">QPID, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.30</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.31</td>
                                    <td className="column100 column2" data-column="column2">QPID, QPID SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.31</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.32</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES, POSTGRES SERVER</td>
                                    <td className="column100 column3" data-column="column3">22</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.32</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1"><a href="https://10.96.69.33:9443/login">10.96.69.33</a></td>
                                    <td className="column100 column2" data-column="column2">OPEN LDAP, MANAGEMENT SERVER</td>
                                    <td className="column100 column3" data-column="column3">22, 9443, 8443</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.33</td>
                                </tr>
                                <tr className="row100">
                                    <td className="column100 column1" data-column="column1">10.96.69.34</td>
                                    <td className="column100 column2" data-column="column2">POSTGRES</td>
                                    <td className="column100 column3" data-column="column3">8079</td>
                                    <td className="column100 column4" data-column="column4">ssh ec2-user@10.96.69.34</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    {/*Jenkins, Gitlab etc*/}
                </div>
            </div>
        </div>

    );
}
export default PlanetsComponent;