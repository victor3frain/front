import React from "react";
import axios from "axios";

function AutotemplateComponent() {

    function TestAutotemplate() {
        axios({
            method: 'post',
            url: "http://10.51.209.88:8285/UpdateLogPaths",
            data: {
                NumeroEmpleado: "1021848",
                MasterKey: "Goodgirlws190319",
                NombreAPI: "elektra-comercio-ordenes-compra-v1",
                IDsTemplate: "10273",
                env: ""
            },
            responseType: 'json'
        }).then(response => {
            alert("Prueba exitosa");
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')',
                target: document.getElementById("curlsDiv")
            })
        });
    }

    return (
        <div>
            <button className="btn btn-primary" onClick={e=>{TestAutotemplate()}}>Probar autotemplate</button>
        </div>
    );
}
export default AutotemplateComponent;