import React, { useState } from 'react';

function ArchitectsComponent(props) {
    const [users, setUsers] = useState({
        team1: [
            {
                email: 'jorge.gomezs@elektra.com.mx',
                name: 'Jorge',
                lastName: 'Gómez De Santos'
            },
            {
                email: 'ecesar.suarezg@elektra.com.mx',
                name: 'César',
                lastName: 'Suárez Guzmán'
            },
            {
                email: 'pedro.garcia@elektra.com.mx',
                name: 'Pedro Vicente',
                lastName: 'Garcia Solis'
            },
            {
                email: 'alejandro.brena@elektra.com.mx',
                name: 'Alejandro',
                lastName: 'Brena Illan'
            }
        ],
        team2: [
            {
                email: 'rrazo@elektra.com.mx',
                name: 'Rodolfo',
                lastName: 'Razo Rodriguez'
            },
            {
                email: 'mimartineza@elektra.com.mx',
                name: 'Miguel',
                lastName: 'Martinez Albino'
            },
            {
                email: 'martin.anchondo@elektra.com.',
                name: 'Martin Geronimo',
                lastName: 'Anchondo Bermudez'
            },
            {
                email: 'david.aguila@elektra.com.mx',
                name: 'David Gerardo',
                lastName: 'Del Aguila Martinez'
            },
            {
                email: 'daniel.badillo@elektra.com.mx',
                name: 'Daniel',
                lastName: 'Badillo Martinez'
            },
            {
                email: 'ivan.lopezcas@elektra.com.mx',
                name: 'Ivan',
                lastName: 'Lopez Casanova'
            },
            {
                email: 'jorge.mejiad@elektra.com.mx',
                name: 'Jorge Luis',
                lastName: 'Mejia Dolores'
            }
        ],
        team3: [
            {
                email: 'edna.patraca@elektra.com.mx',
                name: 'Edna Magdalena',
                lastName: 'Patraca Santiago'
            },
            {
                email: 'oscar.rangel@elektra.com.mx',
                name: 'Oscar Daniel',
                lastName: 'Rangel Martinez'
            },
            {
                email: 'david.aleman@elektra.com.mx',
                name: 'David',
                lastName: 'Aleman Sanchez'
            },
            {
                email: 'lorett.fermin@elektra.com.mx',
                name: 'Lorett',
                lastName: 'Fermin Ramirez'
            },
            {
                email: 'israel.gonzalezl@elektra.com.mx',
                name: 'Israel',
                lastName: 'Gonzalez Luna'
            },
            {
                email: 'oscar.zurita@elektra.com.mx',
                name: 'Oscar Alejandro',
                lastName: 'Zurita Martinez'
            },
            {
                email: 'ubaldo.cruz@elektra.com.mx',
                name: 'Ubaldo',
                lastName: 'Cruz Jose'
            },
            {
                email: 'eaguilare@elektra.com.mx',
                name: 'Eduardo',
                lastName: 'Aguilar Estrada'
            },
            {
                email: 'alan.gonzalezg@elektra.com.mx',
                name: 'Alan Ricardo',
                lastName: 'Gonzalez Guerra'
            },
            {
                email: 'angel.godinezc@elektra.com.mx',
                name: 'Angel Adrian',
                lastName: 'Godinez Cartas'
            }
        ],
        team4: [
            {
                email: 'jose.hernandezo@elektra.com.mx',
                name: 'Jose Emeris',
                lastName: 'Hernandez Ortiz'
            },
            {
                email: 'juan.gonzalezl@elektra.com.mx',
                name: 'Juan Ramón',
                lastName: 'Gonzalez López'
            },
            {
                email: 'kevin.tellez@elektra.com.mx',
                name: 'Kevin',
                lastName: 'Tellez González'
            }
        ],
        team5: [
            {
                email: 'emmanuel.mariscal@elektra.com.mx',
                name: 'Emmanuel',
                lastName: 'Mariscal Lozano'
            }
        ]
    });

    const GetArchitectsComponent = (teamNumber) => {
        var component = [];
        const team = users["team" + teamNumber];
        if (team.length <= 0) {
            component = <h6 className='m-3'>No se encontraron arquitectos registrados</h6>
        }
        else {
            for (var user of users["team" + teamNumber]) {
                component.push(
                    <div className="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                        <div className="card bg-light d-flex flex-fill">
                            <div className="card-header text-muted border-bottom-0">
                                Arquitecto
                            </div>
                            <div className="card-body pt-0">
                                <div className="row">
                                    <div className="col-7">
                                        <h2 className="lead"><b>{user.name} {user.lastName}</b></h2>
                                        <p className="text-muted text-sm"><b>Acerca de: </b> Arquitecto / Desarrollador </p>
                                        <ul className="ml-4 mb-0 fa-ul text-muted">
                                            <li className="small"><span className="fa-li"><i className="fas fa-lg fa-building" /></span> Torre Área, piso 4, CDMX</li>
                                            <li className="small"><span className="fa-li"><i className="fa fa-lg fa-envelope" /></span>{user.email}</li>
                                        </ul>
                                    </div>
                                    <div className="col-5 text-center">
                                        <img src="../../media/users/user_logo.png" alt="user-avatar" className="img-circle img-fluid" />
                                    </div>
                                </div>
                            </div>
                            {/*
                            <div className="card-footer">
                                <div className="text-right">
                                    <a href="#" className="btn btn-sm bg-teal">
                                        <i className="fas fa-comments" />
                                    </a>
                                    <a href="#" className="btn btn-sm btn-primary">
                                        <i className="fas fa-user" /> View Profile
                                    </a>
                                </div>
                            </div>
                        */}
                        </div>
                    </div>
                );
            }
        }
        return component;
    }

    return (
        <div className="card card-primary card-outline">
            <div className="card-header">
                <h3 className="card-title">
                    <i className="fa fa-users mr-1" />
                    Arquitectos
                </h3>
            </div>
            <div className="card-body">
                <h4>Equipos</h4>
                <ul className="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="custom-content-below-1-tab" data-toggle="pill" href="#custom-content-below-1" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Jonathan Germain</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="custom-content-below-2-tab" data-toggle="pill" href="#custom-content-below-2" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Rodolfo Razo</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="custom-content-below-3-tab" data-toggle="pill" href="#custom-content-below-3" role="tab" aria-controls="custom-content-below-messages" aria-selected="false">Francisco Gerardo</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="custom-content-below-4-tab" data-toggle="pill" href="#custom-content-below-4" role="tab" aria-controls="custom-content-below-settings" aria-selected="false">Erick Carrillo</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="custom-content-below-5-tab" data-toggle="pill" href="#custom-content-below-5" role="tab" aria-controls="custom-content-below-settings" aria-selected="false">Jonathan Alcántara</a>
                    </li>
                </ul>
                <div className="tab-content" id="custom-content-below-tabContent">
                    <div className="tab-pane fade show active" id="custom-content-below-1" role="tabpanel" aria-labelledby="custom-content-below-1-tab">
                        <div className='w-100 mt-3 row'>{GetArchitectsComponent(1)}</div>
                    </div>
                    <div className="tab-pane fade" id="custom-content-below-2" role="tabpanel" aria-labelledby="custom-content-below-2-tab">
                        <div className='w-100 mt-3 row'>{GetArchitectsComponent(2)}</div>
                    </div>
                    <div className="tab-pane fade" id="custom-content-below-3" role="tabpanel" aria-labelledby="custom-content-below-3-tab">
                        <div className='w-100 mt-3 row'>{GetArchitectsComponent(3)}</div>
                    </div>
                    <div className="tab-pane fade" id="custom-content-below-4" role="tabpanel" aria-labelledby="custom-content-below-4-tab">
                        <div className='w-100 mt-3 row'>{GetArchitectsComponent(4)}</div>
                    </div>
                    <div className="tab-pane fade" id="custom-content-below-5" role="tabpanel" aria-labelledby="custom-content-below-5-tab">
                        <div className='w-100 mt-3 row'>{GetArchitectsComponent(5)}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ArchitectsComponent;