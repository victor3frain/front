import React, { useEffect, useState } from 'react'
import AssigmentComponent from './AssigmentComponent.js';
import MainComponent from '../MainComponents/MainComponent.js';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';
import Utils from '../Utils/Utils.js';
import Settings from '../Utils/configsettings.js';


const Assigments = () => {
    let History = useHistory();
    const [session, setSession] = useState({ User: {} });
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    const SessionInterval = setInterval(() => {
        if (!window.location.pathname.toUpperCase().includes("DEVOPS_ASSINGS")) {
            clearInterval(SessionInterval)
            return;
        }
        GetUserInfo();
    }, 600000);

    function GetUserInfo() {
        var usr = Utils.getCookie("user");
        if (!usr)
            Restart(usr)
        else
            axios({
                method: 'get',
                url: Settings.GetApiIp() + "/CoeTicketSec/user/info/" + usr,
                responseType: 'json'
            }).then(response => {
                var data = response.data;
                if (data.error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error: ' + data.error
                    });
                }
                else {
                    var userInfo = data.body;
                    if (!userInfo.User) {
                        userInfo = { User: {} };
                    }
                    else
                        setSession(userInfo);
                }
            }).catch(error => {
                console.log(error.message)
                Toast.fire({
                    icon: 'error',
                    title: 'Ocurrio un error al obtener la información del usuario'
                })
            });
    }

    function CheckSession() {
        GetUserInfo();
    }

    function Restart(userInfo) {
        if (!userInfo) userInfo = { User: {} };
        Utils.setCookie("user", Utils.encodeBase64(userInfo.User.email), -1);
        History.push("/");
    }

    useEffect(() => {
        CheckSession();
    }, []);

    return (
        <MainComponent
            User={session.User}
            checkingSessionId={SessionInterval}
            Title="Asignación de tareas"
            Route={[
                { Title: "Inicio", Route: "/home" },
                { Title: "DevOps", Route: "/devops" },
                { Title: "Asignación de tareas", Route: "/devops_asigns" }]}
            Component={<AssigmentComponent />} />)
}
export default Assigments;