import React, { useEffect, useState } from 'react';
import MainComponent from '../MainComponents/MainComponent';
import APIComponent from './APIComponent';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';
import Utils from '../Utils/Utils.js';
import Settings from '../Utils/configsettings.js';

const API = () => {
    let History = useHistory();
    const [session, setSession] = useState({ User: {} });
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    const SessionInterval = setInterval(() => {
        if (!window.location.pathname.toUpperCase().includes("DEVELOPMENT_API")) {
            clearInterval(SessionInterval)
            return;
        }
        GetUserInfo();
    }, 600000);

    function GetUserInfo() {
        var usr = Utils.getCookie("user");
        if (!usr)
            Restart(usr)
        else
            axios({
                method: 'get',
                url: Settings.GetApiIp() + "/CoeTicketSec/user/info/" + usr,
                responseType: 'json'
            }).then(response => {
                var data = response.data;
                if (data.error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error: ' + data.error
                    });
                }
                else {
                    var userInfo = data.body;
                    if (!userInfo.User) {
                        Restart();
                    }
                    else
                        setSession(userInfo);
                }
            }).catch(error => {
                console.log(error.message)
                Toast.fire({
                    icon: 'error',
                    title: 'Ocurrio un error al obtener la información del usuario'
                })
            });
    }

    function CheckSession() {
        GetUserInfo();
    }

    function Restart(userInfo) {
        if (!userInfo)
            userInfo = { User: {} };
        Utils.setCookie("user", Utils.encodeBase64(userInfo.User.email), -1);
        History.push("/");
    }

    useEffect(() => {
        CheckSession();
    }, []);

    return (
        <MainComponent
            User={session.User}
            checkingSessionId={SessionInterval}
            Title={"APIs"}
            Route={[
                { Title: "Inicio", Route: "/home" },
                { Title: "Desarrolladores", Route: "/developers" },
                { Title: "APIs", Route: "/developers_apis" }]}
            Component={<APIComponent user={session.User} />}
        />
    )
}
export default API;