import React, { useState } from "react";
//API, Recursos, Microservicios
import { Box, TextField, Select, Chip, OutlinedInput, MenuItem, FormControl, InputLabel, IconButton } from '@mui/material';
import { Api } from '@mui/icons-material';
import DataTable from "../Utils/GenericComponents/DataTable";
import { Delete, Edit, Visibility } from '@mui/icons-material';


function APIComponent() {
    const [apiData, setApiData] = useState({
        resource: 0,
        name: '',
        microservice: '',
        resourceName: ''
    });

    const columns = [
        { field: '_id', headerName: 'ID', width: 10 },
        {
            field: 'name',
            headerName: 'Nombre',
            width: 150,
            type: "string",
            description: "Nombre del API"
        },
        {
            field: 'microservice',
            headerName: 'Microservicio',
            width: 150,
            description: "Nombre del microservicio"
        },
        {
            field: 'lastactivity',
            headerName: 'Última actividad',
            width: 200,
            type: "date",
            description: "Fecha registrada de la última actividad",
            valueGetter: ({ value }) => value && new Date(value),
        },
        {
            field: "edit",
            headerName: "Editar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    alert("Editando ...")
                };

                return (
                    <IconButton aria-label="Editar" onClick={onClick}>
                        <Edit />
                    </IconButton>)
            }
        },
        {
            field: "delete",
            headerName: "Eliminar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    alert("Eliminando ...")
                };

                return (
                    <IconButton aria-label="Eliminar" onClick={onClick}>
                        <Delete />
                    </IconButton>)
            }
        },
        {
            field: "showDetails",
            headerName: "Ver detalles",
            sortable: false,
            width: 100,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    alert("Viendo detalles ...")
                };

                return (
                    <IconButton aria-label="Mostrar detalles" onClick={onClick}>
                        <Visibility />
                    </IconButton>)
            }
        }
    ];

    const apiForm = (
        <div className="card card-primary">
            <div className="card-header">
                <h3 className="card-title">APIs</h3>
            </div>
            <div className="card-body ml-2 row">
                <div className="card col-4">
                    <div className="card-header">
                        <h3 className="card-title">
                            Registrar API
                        </h3>
                    </div>
                    <div className="card-body">
                        <div>
                            <form onSubmit={e => alert("Subiendo ...")}>

                                <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                                    <Api sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
                                    <TextField label="Nombre" variant="standard" value={apiData.name} required />
                                </Box>
                                <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                                    <Api sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
                                    <TextField label="Microservicio" variant="standard" value={apiData.microservice} required />
                                </Box>
                                <div className="row mt-3">
                                    <div className="col-7">
                                        <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                                            <Api sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
                                            <TextField label="Nombre del recurso" variant="standard" value={apiData.resourceName} required />
                                        </Box>
                                    </div>
                                    <div className="col-5">
                                        <FormControl fullWidth>
                                            <InputLabel>Recursos</InputLabel>
                                            <Select
                                                className="mr-1 flex-left"
                                                value={apiData.resource}
                                                onChange={e => {
                                                    setApiData(prevState => {
                                                        let api = Object.assign({}, apiData);
                                                        api.resource = e.target.value
                                                        return api
                                                    })
                                                }}
                                                input={<OutlinedInput label="Recursos" />}
                                                required
                                            >
                                                <MenuItem value={0} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#61affe",
                                                        color: "white"
                                                    }}
                                                        label="GET"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={1} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#49cc90",
                                                        color: "white"
                                                    }}
                                                        label="POST"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={2} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#fca130",
                                                        color: "white"
                                                    }}
                                                        label="PUT"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={3} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#f93e3e",
                                                        color: "white"
                                                    }}
                                                        label="DELETE"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={4} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#0d5aa7",
                                                        color: "white"
                                                    }}
                                                        label="OPTIONS"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={5} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#3be2c2",
                                                        color: "white"
                                                    }}
                                                        label="PATCH"
                                                    />
                                                </MenuItem>
                                                <MenuItem value={6} >
                                                    <Chip className="ml-3" style={{
                                                        backgroundColor: "#8e3df7",
                                                        color: "white"
                                                    }}
                                                        label="HEAD"
                                                    />
                                                </MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>

                                <input className="btn btn-primary" type="submit" value="Guardar" />
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-8">
                    <DataTable height="400px" title="Lista de APIs" toolBar={true} dataAddress="CoeTicketAPI/api/getapilist" columns={columns} />
                </div>
            </div>
        </div>
    );

    return (apiForm);
}
export default APIComponent;