const SettingsDetails = {
    IpAddressAPI: "https://git.devsecops.com.mx",
    Teams: [    
        {
            id: 0,
            name: "DevOps",
            color: "#266f8d",
            textColor: "white"
        },
        {
            id: 1,
            name: "Jonathan Germain",
            color: "#d1f6b1",
            textColor: "black"
        },
        {
            id: 2,
            name: "Rodolfo Razo",
            color: "#bbcff2",
            textColor: "black"
        },
        {
            id: 3,
            name: "Francisco Gerardo",
            color: "#f7f392",
            textColor: "black"
        },
        {
            id: 4,
            name: "Erick Carrillo",
            color: "#fe9900",
            textColor: "black"
        },
        {
            id: 5,
            name: "Jonathan Alcántara",
            color: "#9f7fb1",
            textColor: "black"
        },
        {
            id: 6,
            name: "Ezra Garcia",
            color: "#2cc9be",
            textColor: "white"
        },
        {
            id: 7,
            name: "Juan Ferreira",
            color: "#cdf2d7",
            textColor: "black"
        },
        {
            id: 8,
            name: "SCRUM",
            color: "#dc6656",
            textColor: "black"
        }
    ],
    Resources:{
        Api: "api/"
    }
};

export default class Settings {
    static GetSettings = () => {
        return SettingsDetails
    }

    static GetApiIp = () => {
        return SettingsDetails.IpAddressAPI;
    }

    static GetTeams = () => {
        return SettingsDetails.Teams;
    }
}
