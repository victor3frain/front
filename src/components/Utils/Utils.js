const TOTAL_BACKGROUNDS = 3;

const MakeBackgroundNumber = (min, max) => {
  var number = Math.floor(Math.random() * ((max + 1) - min)) + min;
  if (number > max)
    return MakeBackgroundNumber(1, TOTAL_BACKGROUNDS)
  return number
}


function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

//Auxiliar function to get the second ocurrence into a string
function GetCharPosition(string, char, ocurrence) {
  return string.split(char, ocurrence).join(char).length;
}

function encodeBase64(value) {
  return window.btoa(unescape(encodeURIComponent(value)));
}

export default class Utils {
  static GetBackground = () => {
    return "url(media/backgrounds/login_background_" + MakeBackgroundNumber(1, TOTAL_BACKGROUNDS) + ".jpg"
  }

  static setCookie = (name, value, exdays) => {
    setCookie(name, value, exdays);
  }

  static getCookie = (name) => {
    return getCookie(name);
  }

  static encodeBase64 = (value) => {
    return encodeBase64(value);
  }

  static getCharPosition = (string, char, ocurrence) => {
    return GetCharPosition(string, char, ocurrence);
  }
}