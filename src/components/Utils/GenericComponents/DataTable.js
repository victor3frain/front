import React, { useState, useEffect } from 'react';

//MUI
import {
    DataGrid, esES, GridToolbarContainer, GridToolbarColumnsButton,
    GridToolbarFilterButton, GridToolbarExport, GridToolbarDensitySelector
} from '@mui/x-data-grid';

//Sweet Alert
import Swal from 'sweetalert2';

//Settings
import Settings from '../configsettings';

//axios
import axios from 'axios';

const DataTable = (props) => {
    const [isLoading, setIsLoading] = useState(true);
    const [pageInfo, setPageInfo] = useState({
        totalRowCount: 0,
        page: 0,
        pageSize: 10
    });

    const [data, setData] = useState([])
    const initialState = {
        pageSize: 10
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });


    // Some API clients return undefined while loading
    // Following lines are here to prevent `rowCountState` from being undefined during the loading
    const [rowCountState, setRowCountState] = useState(
        pageInfo?.totalRowCount || 0,
    );

    const CustomToolbar = () => {
        var component = []

        if (props.toolBar === true)
            component = (
                <GridToolbarContainer>
                    <GridToolbarColumnsButton />
                    <GridToolbarFilterButton />
                    <GridToolbarDensitySelector />
                    <GridToolbarExport />
                </GridToolbarContainer>
            );
        else if (typeof props.toolBar === "object")
            component = props.toolBar;

        return component;
    }

    //Validations
    function thereIsAnError() {
        var error = false;
        error = !props.columns

        return error;
    }

    useEffect(() => {
        setRowCountState((prevRowCountState) =>
            pageInfo?.totalRowCount !== undefined
                ? pageInfo?.totalRowCount
                : prevRowCountState,
        );
    }, [pageInfo?.totalRowCount, setRowCountState]);

    function GetData() {
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/" + props.dataAddress + "/" + pageInfo.pageSize + "/" + pageInfo.page,
            responseType: 'json'
        }).then(response => {
            var responseData = response.data;
            if (responseData.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error al obtener los registros: ' + data.error
                });
            }
            else {
                const page = responseData.body
                setPageInfo(prevState => {
                    let PI = Object.assign({}, pageInfo);
                    PI.totalRowCount = page.count;
                    return PI;
                });
                setData(page.data);
                setIsLoading(false);
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener el listado de APIs (' + error.message + ')'
            })
        });
    }

    useEffect(() => {
        if (!thereIsAnError()) {
            GetData();
        }
    }, [])

    return (
        <div className='card'>
            <div className='card-header'>
                <h3 className="card-title">
                    {props.title}
                </h3>
            </div>
            <div className="card-body">
                <div style={{ height: props.height }}>
                    {
                        !thereIsAnError() ?
                            <DataGrid
                                localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                                rows={data}
                                rowCount={rowCountState}
                                loading={isLoading}
                                rowsPerPageOptions={[10, 25, 50, 100]}
                                pagination
                                page={pageInfo.page}
                                pageSize={pageInfo.pageSize}
                                paginationMode="server"
                                onPageChange={(newPage) => setPageInfo(prevState => {
                                    let PI = Object.assign({}, pageInfo);
                                    PI.page = newPage;
                                    return PI;
                                })}
                                onPageSizeChange={(newPageSize) => setPageInfo(prevState => {
                                    let PI = Object.assign({}, pageInfo);
                                    PI.pageSize = newPageSize;
                                    return PI;
                                })}
                                columns={props.columns}
                                initialState={initialState}
                                components={{
                                    Toolbar: CustomToolbar
                                }}
                                getRowId={(row) => row._id}
                            />
                            :
                            <h3>Ocurrió un error inesperado, por favor consulte la información enviada dentro de los componentes</h3>
                    }
                </div>
            </div>
        </div>
    );

}
export default DataTable;