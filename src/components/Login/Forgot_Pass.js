import React from 'react';
import Utils from '../Utils/Utils.js';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

function Forgot_Pass() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    function SendMail(event) {
        event.preventDefault()
        Toast.fire({
            icon: 'success',
            title: 'Correo enviado'
        })
    }

    return (
        <div className="hold-transition login-page" style={{
            backgroundImage: Utils.GetBackground(),
            backgroundColor: "#edeae9",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover"
        }}>
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to="/" className="h1">
                            <img src="media/logos/logo_coe_apis.png" alt="logo_coe_apis" />
                        </Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Ingresa tu correo.</p>
                        <form onSubmit={e=>SendMail(e)} action="/" method="post">
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email" required />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Enviar</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                        <p className="mt-3 mb-1">
                            <Link to="/">Regresar al inicio</Link>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>
        </div>
    );
}
export default Forgot_Pass;