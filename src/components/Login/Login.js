import React, { useState, useEffect, useRef } from 'react';
import Utils from '../Utils/Utils.js';
import Settings from '../Utils/configsettings.js';
import { useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';

function Login() {
    let History = useHistory();
    const emailInput = useRef();
    const passInput = useRef();
    const rememberUser = useRef();

    //State variables
    const [showLogin, setShowLogin] = useState(false);

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    const AuthenticateUser = async (email, password, rememberUsr) => {
        const usr_email = Utils.encodeBase64(email.trim())
        const usr_password = Utils.encodeBase64(encodeURIComponent(password))
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketSec/login/authenticate/" + usr_email + "/" + usr_password + "/" + rememberUsr,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                })
            }
            else {
                if (typeof response.data.body !== 'object')
                    data = JSON.parse(response.data.body);

                if (data.body.login) {
                    var time2Expire = !rememberUsr ? 0.0208333 : 365;
                    Utils.setCookie("user", Utils.encodeBase64(data.user), time2Expire);
                    History.push("/home")
                }
                else {
                    Toast.fire({
                        icon: 'error',
                        title: 'E-mail o contraseña incorrecta, por favor intenta de nuevo'
                    })
                }
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrio un error inesperado'
            })
        });
    }

    function checkCredentials(event) {
        event.preventDefault();
        const email = emailInput.current.value;
        const password = passInput.current.value;
        AuthenticateUser(email, password, rememberUser.current.checked);
    }

    function checkSession() {
        var usr = Utils.getCookie("user");
        if (usr) {
            axios({
                method: 'get',
                url: Settings.GetApiIp() + "/CoeTicketSec/login/checksession/" + usr,
                responseType: 'json'
            }).then(response => {
                var data = response.data;
                if (data.error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error al obtener la sesión: ' + data.error
                    })
                } else if (data.body.session && data.body.ipSession) {
                    History.push("/home");
                }
                else {
                    setShowLogin(true);
                }
            }).catch(error => {
                console.log(error.message)
                Toast.fire({
                    icon: 'error',
                    title: 'Ocurrio un error inesperado'
                })
            });
        }
        else {
            setShowLogin(true);
        }
    }

    useEffect(() => {
        checkSession()
    }, [])

    return (
        <div className="login-page" style={{
            backgroundImage: Utils.GetBackground(),
            backgroundColor: "#edeae9",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover"
        }}>
            <div className="login-box">
                {showLogin ?
                    <div className="card card-outline card-primary">
                        <div className="card-header text-center">
                            <Link to="/" className="h1">
                                <img src="media/logos/logo_coe_apis.png" alt="logo_coe_apis" />
                            </Link>
                        </div>
                        <div className="card-body">
                            <p className="login-box-msg">Iniciar sesión</p>
                            <form onSubmit={e => checkCredentials(e)} action="/" method="get" className="login_form">
                                <div className="input-group mb-3">
                                    <input
                                        key="emailInputKey"
                                        ref={emailInput}
                                        type="email"
                                        className="form-control"
                                        placeholder="E-Mail"
                                        required
                                    />
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fas fa-user" />
                                        </div>
                                    </div>
                                </div>
                                <div className="input-group mb-3">
                                    <input
                                        key="passInputKey"
                                        ref={passInput}
                                        type="password"
                                        className="form-control"
                                        placeholder="Contraseña"
                                        required
                                    />
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fas fa-lock" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-8">
                                        <div className="icheck-primary">
                                            <input ref={rememberUser} type="checkbox" id="remember" className="login_recovery" />
                                            <label htmlFor="remember">
                                                Recordarme
                                            </label>
                                        </div>
                                    </div>
                                    {/* /.col */}
                                    <div className="col-4">
                                        <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
                                    </div>
                                    {/* /.col */}
                                </div>
                            </form>
                            <p className="mb-1 login_second_options_label">
                                <Link to="/forgotpass">Olvidé mi contraseña</Link>
                            </p>
                            <p className="mb-0 login_second_options_label">
                                <Link to="/newaccount" className="text-center">Registrar nueva cuenta</Link>
                            </p>
                        </div>
                        {/* /.card-body */}
                    </div> : <div></div>
                }
            </div>
        </div>
    )
}
export default Login;