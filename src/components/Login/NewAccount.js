import React from 'react';
import Utils from '../Utils/Utils';
import { Link } from 'react-router-dom';

const NewAccount = () => {
    return (
        <div className='login-page' style={{
            backgroundImage: Utils.GetBackground(),
            backgroundColor: "#edeae9",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover"
        }}>
            <div className="register-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <img src="media/logos/logo_coe_apis.png" alt="logo_coe_apis" />
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Registra tu cuenta</p>
                        <form onSumbit={e => e.preventDefault()} action="/" method="get">
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Nombre completo" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" placeholder="Ingresa tu contraseña" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" placeholder="Ingresa nuevamente tu contraseña" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-7">
                                    <Link to="/" className="text-center">Ya tengo una cuenta</Link>
                                </div>
                                {/* /.col */}
                                <div className="col-5">
                                    <button type="submit" className="btn btn-primary btn-block">Registrarse</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                    </div>
                    {/* /.form-box */}
                </div>{/* /.card */}
            </div>
        </div>
    );
}
export default NewAccount;