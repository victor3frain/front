import React from 'react';
import { Link } from 'react-router-dom';
import Utils from '../Utils/Utils';

function Recover_Pass() {
    return (
        <div className="hold-transition login-page" style={{
            backgroundImage: Utils.GetBackground(),
            backgroundColor: "#edeae9",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover"
        }}>
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to="/">
                            <img src="media/logos/logo_coe_apis.png" alt="logo_coe_apis" />
                        </Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Estás sólo a un paso de recuperar tu contraseña.</p>
                        <form action="login.html" method="post">
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" placeholder="Contraseña" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" placeholder="Confirmar contraseña" />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Cambiar contraseña</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                        <p className="mt-3 mb-1">
                            <Link to="/">Ir a inicio</Link>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>
        </div>
    );
}
export default Recover_Pass;