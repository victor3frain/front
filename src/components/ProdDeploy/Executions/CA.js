import React, { useState, useEffect } from 'react';

//Settings
import Settings from '../../Utils/configsettings.js';

//Axios
import axios from 'axios';

//MUI
import {
    FormControl, FormLabel, Checkbox, FormControlLabel, TextareaAutosize, IconButton, Button, Radio, RadioGroup
} from '@mui/material';
import {
    DataGrid, esES, GridToolbarContainer, GridToolbarColumnsButton,
    GridToolbarFilterButton, GridToolbarExport, GridToolbarDensitySelector
} from '@mui/x-data-grid';
import { Delete, Edit, Visibility } from '@mui/icons-material';

//Sweet alert 2
import Swal from 'sweetalert2';

function CA(props) {
    const [CAData, setCAData] = useState({
        name: "",
        repo: "",
        type: "NEW",
        enviroments: {
            prodInt: false,
            prodExt: false,
            prodIntOn: false,
            prodExtOn: false
        },
        origin: "uat",
        details: ""
    });
    const [data, setData] = useState([]);
    const [AddedCAView, setAddedCAView] = useState(true);
    const [editMode, setEditMode] = useState(false);

    function AddingCA() {
        var values = {
            icon: "",
            title: "",
            text: ""
        }
        if (!CAData.enviroments.prodInt && !CAData.enviroments.prodExt && !CAData.enviroments.prodIntOn && !CAData.enviroments.prodExtOn) {
            values.icon = "error";
            values.title = "Error";
            values.text = "No fue seleccionado ningún ambiente";
        }
        else {
            var newData = [...data]
            newData.push({ id: data.length + 1, ...CAData })
            values.icon = "success";
            values.title = "Agregado";
            values.text = "Ambiente agregado";
            setData(newData)
            ResetValues();
        }
        Toast.fire({
            ...values,
            target: document.getElementById('generalContainer'),
        })
    }

    function ResetValues() {
        setCAData({
            name: "",
            repo: "",
            type: "NEW",
            enviroments: {
                prodInt: false,
                prodExt: false,
                prodIntOn: false,
                prodExtOn: false
            },
            origin: "uat",
            details: ""
        });
        setAddedCAView(false);
        setEditMode(false);
    }

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'name',
            headerName: 'Nombre',
            width: 150,
            type: "string",
            sortable: false,
            description: "Nombre del configurador de ambiente"
        },
        {
            field: 'repo',
            headerName: 'Link',
            width: 150,
            description: "Link del CA",
            renderCell: (params) => {
                return <a href={params.row.repo} target="_BLANK" rel="noopener noreferrer">Link del repo</a>
            }
        },
        {
            field: 'enviroments',
            headerName: 'Ambientes',
            width: 400,
            type: "string",
            description: "Ambientes de desarrollo",
            valueGetter: (params) => {
                var enviroments = [];
                const env = params.row.enviroments;
                if (env.prodInt)
                    enviroments.push("prod-int");
                if (env.prodExt)
                    enviroments.push("prod-ext");
                if (env.prodIntOn)
                    enviroments.push("prod-int-onpremise");
                if (env.prodExtOn)
                    enviroments.push("prod-ext-onpremise");

                var value = "";
                for (let envName of enviroments)
                    value += envName + ", "
                value = value.substring(0, value.length - 2);
                return "[" + value + "]"
            }
        },
        {
            field: "edit",
            headerName: "Editar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setCAData(params.row)
                    setAddedCAView(false)
                    setEditMode(true);
                };

                return (
                    <IconButton aria-label="Editar" onClick={onClick}>
                        <Edit />
                    </IconButton>)
            }
        },
        {
            field: "delete",
            headerName: "Eliminar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    DeleteCA(params.row.id)
                };

                return (
                    <IconButton aria-label="Eliminar" onClick={onClick}>
                        <Delete />
                    </IconButton>)
            }
        },
        {
            field: "showDetails",
            headerName: "Ver detalles",
            sortable: false,
            width: 100,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setCAData(params.row);
                    setEditMode("show");
                    setAddedCAView(false);
                };

                return (
                    <IconButton aria-label="Mostrar detalles" onClick={onClick}>
                        <Visibility />
                    </IconButton>)
            }
        }
    ];

    function CustomToolbar() {
        return (
            <GridToolbarContainer>
                <GridToolbarColumnsButton />
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
                <GridToolbarExport />
            </GridToolbarContainer>
        );
    }

    function UpdateCA() {
        var newData = [...data];
        var caIndex = data.findIndex(envConf => envConf.id === CAData.id)
        newData[caIndex] = CAData;
        setData(newData);
        Toast.fire({
            title: "Actualización",
            text: "Actualización correcta",
            icon: "success",
            target: document.getElementById('generalContainer')
        })
    }

    function DeleteCA(id) {
        const index = data.findIndex(ca => ca.id === id);
        var newData = [...data];
        newData.splice(index - 1, 1);
        setData(newData);
    }

    const CAComponent = (
        <form onSubmit={e => { e.preventDefault(); editMode ? UpdateCA() : AddingCA(); }}>
            <div className="form-group w-100">
                <div className="row">
                    <div className="col-6">
                        <FormLabel>Nombre del configurador de ambiente</FormLabel>
                        <input
                            type="text"
                            value={CAData.name}
                            onChange={e => setCAData(prevState => {
                                let ca = Object.assign({}, CAData);
                                ca.name = e.target.value
                                return ca
                            }
                            )}
                            className="form-control form-control-border mb-3"
                            placeholder="Nombre"
                            disabled={editMode === "show"}
                            required
                        />
                        <FormLabel>Link del repositorio</FormLabel>
                        <input
                            type="text"
                            value={CAData.repo}
                            onChange={e => setCAData(prevState => {
                                let ca = Object.assign({}, CAData);
                                ca.repo = e.target.value
                                return ca
                            })}
                            disabled={editMode === "show"}
                            className="form-control form-control-border mb-3"
                            placeholder="Link"
                            required
                        />
                        <FormControl>
                            <FormLabel className='w-100'>Tipo de despliegue</FormLabel>
                            <RadioGroup
                                row
                                aria-labelledby="demo-row-radio-buttons-group-label"
                                name="row-radio-buttons-group"
                            >
                                <FormControlLabel
                                    value="1"
                                    control={<Radio />}
                                    label="Nuevo"
                                    checked={CAData.type === "NEW"}
                                    onClick={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.type = "NEW"
                                        return ca
                                    })}
                                    disabled={editMode === "show"}
                                />
                                <FormControlLabel
                                    value="2"
                                    control={<Radio />}
                                    label="Actualización"
                                    checked={CAData.type === "UPDATE"}
                                    onClick={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.type = "UPDATE"
                                        return ca
                                    })}
                                    disabled={editMode === "show"}
                                />
                            </RadioGroup>
                        </FormControl>
                    </div>
                    <div className="col-6">
                        <FormControl>
                            <FormLabel className='w-100'>Origen</FormLabel>
                            <RadioGroup
                                row
                                aria-labelledby="demo-row-radio-buttons-group-label"
                                name="row-radio-buttons-group"
                            >
                                <FormControlLabel
                                    value="1"
                                    control={<Radio />}
                                    label="AWS"
                                    checked={CAData.origin === "uat"}
                                    onClick={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.origin = "uat"
                                        return ca
                                    })}
                                    disabled={editMode === "show"}
                                />
                                <FormControlLabel
                                    value="2"
                                    control={<Radio />}
                                    label="On Premise"
                                    checked={CAData.origin === "uat-onpremise"}
                                    onClick={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.origin = "uat-onpremise"
                                        return ca
                                    })}
                                    disabled={editMode === "show"}
                                />
                            </RadioGroup>
                            <FormLabel className='w-100'>Destinos</FormLabel>
                            <div className='row'>
                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={CAData.enviroments.prodInt} disabled={editMode === "show"} onChange={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.enviroments.prodInt = e.target.checked
                                        return ca
                                    })} name="Prod-Int" />}
                                    label="Prod-Int"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={CAData.enviroments.prodExt} disabled={editMode === "show"} onChange={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.enviroments.prodExt = e.target.checked
                                        return ca
                                    })} name="Prod-Ext" />}
                                    label="Prod-Ext"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={CAData.enviroments.prodIntOn} disabled={editMode === "show"} onChange={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.enviroments.prodIntOn = e.target.checked
                                        return ca
                                    })} name="Prod-Int-OnPromise" />}
                                    label="Prod-Int-OnPremise"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={CAData.enviroments.prodExtOn} disabled={editMode === "show"} onChange={e => setCAData(prevState => {
                                        let ca = Object.assign({}, CAData);
                                        ca.enviroments.prodExtOn = e.target.checked
                                        return ca
                                    })} name="Prod-Ext-OnPremise" />}
                                    label="Prod-Ext-OnPremise"
                                />
                            </div>
                            <span>
                                {editMode === "show" ? [] : <input className='btn btn-primary col-2' type="submit" value={!editMode ? "Agregar" : "Actualizar"} />}
                                {
                                    editMode && editMode !== "show" ?
                                        <Button
                                            className="ml-5"
                                            color='error'
                                            variant='contained'
                                            onClick={e => { ResetValues(); setAddedCAView(true) }}>Cancelar</Button> :
                                        <Button
                                            className="ml-5"
                                            color='secondary'
                                            variant='contained'
                                            onClick={e => { setAddedCAView(true); setEditMode(false) }}>Ver resumen</Button>
                                }
                            </span>
                        </FormControl>
                    </div>
                </div>
                <div>
                    <FormLabel>Detalles</FormLabel>
                    <TextareaAutosize
                        minRows={10}
                        className="w-100"
                        aria-label="Detalles"
                        value={CAData.details}
                        onChange={e => setCAData(prevState => {
                            let ca = Object.assign({}, CAData);
                            ca.details = e.target.value
                            return ca
                        })}
                        disabled={editMode === "show"}
                        placeholder={props.Type.toUpperCase() === "AMBIENTE" ? "KVM y detalles del configurador de ambiente" : "Detalles"}
                    />
                </div>
            </div>
        </form>
    );

    function SaveCA() {
        var dataCA = {
            id: props.DeployDetalis._id,
            Specifications: {
                CA: {}
            }
        }
        dataCA.Specifications.CA[props.Type] = data;

        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/setSpec/",
            data: dataCA,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'CA' + (dataCA.Specifications.CA[props.Type].length > 1 ? 's' : '') + ' modificado' + (dataCA.Specifications.CA[props.Type].length > 1 ? 's' : ''),
                    target: document.getElementById("generalContainer")
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    const AddedCAComponent = (
        <div className='w-100' style={{ height: 350 }}>
            <Button
                className="mb-2"
                color='primary'
                variant='contained'
                onClick={e => { ResetValues() }}>Agregar CA</Button>
            <Button
                className="ml-5 mb-2"
                color='success'
                variant='contained'
                onClick={e => {
                    Swal.fire({
                        title: '¿Estás seguro que deseas confirmar los cambios generados?',
                        text: "Se guardarán los Configuradores de Ambiente modificados",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: 'green',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        target: document.getElementById('sweetContainer')
                    }).then((result) => {
                        if (result.isConfirmed) {
                            SaveCA();
                        }
                    })
                    setAddedCAView(true);
                    setEditMode(false)
                }}>Confirmar</Button>
            <DataGrid
                localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                rows={data}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                components={{
                    Toolbar: CustomToolbar
                }}
            />
        </div>
    )

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 750,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    useEffect(() => {
        if (props.SelectedCA) {
            setCAData(props.SelectedCA);
            setData(props.CurrentCA[props.Type]);
            setEditMode(true);
            setAddedCAView(false);
        }
    }, [])

    return (
        <div id="generalContainer" style={
            {
                overflowX: 'hidden',
                overflowY: 'auto',
                maxHeight: '600px'
            }
        }>
            {AddedCAView ? AddedCAComponent : CAComponent}
            <div id="sweetContainer" style={{
                zIndex: 300000
            }}></div>
        </div>
    )
}
export default CA;