import React, { useState, useEffect } from 'react';

//Settings
import Settings from '../../Utils/configsettings.js';

//MUI
import {
    FormLabel, Checkbox, FormControlLabel, TextareaAutosize, IconButton, Button
} from '@mui/material';
import {
    DataGrid, esES, GridToolbarContainer, GridToolbarColumnsButton,
    GridToolbarFilterButton, GridToolbarExport, GridToolbarDensitySelector
} from '@mui/x-data-grid';
import { Delete, Edit, Visibility } from '@mui/icons-material';

//Axios
import axios from 'axios';

//Sweet alert 2
import Swal from 'sweetalert2';

function JWT(props) {
    const [JWTData, setJWTData] = useState({
        appName: "",
        apiName: "",
        enviroments: {
            prodInt: false,
            prodExt: false,
            prodIntOn: false,
            prodExtOn: false
        },
        curls: {
            firstCurl: "",
            secondCurl: "",
            thirdCurl: ""
        },
        details: ""
    });
    const [editMode, setEditMode] = useState(true);
    const [data, setData] = useState([]);
    const [AddedJWTView, setAddedJWTView] = useState(true);

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'appName',
            headerName: 'Nombre del App',
            width: 150,
            type: "string",
            sortable: false,
            description: "Nombre del App"
        },
        {
            field: 'apiName',
            headerName: 'Nombre del API',
            width: 150,
            type: "string",
            sortable: false,
            description: "Nombre del API"
        },
        {
            field: 'enviroments',
            headerName: 'Ambientes',
            width: 400,
            type: "string",
            description: "Ambientes de desarrollo",
            valueGetter: (params) => {
                var enviroments = [];
                const env = params.row.enviroments;
                if (env.prodInt)
                    enviroments.push("prod-int");
                if (env.prodExt)
                    enviroments.push("prod-ext");
                if (env.prodIntOn)
                    enviroments.push("prod-int-onpremise");
                if (env.prodExtOn)
                    enviroments.push("prod-ext-onpremise");

                var value = "";
                for (let envName of enviroments)
                    value += envName + ", "
                value = value.substring(0, value.length - 2);
                return "[" + value + "]"
            }
        },
        {
            field: "edit",
            headerName: "Editar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setJWTData(params.row)
                    setAddedJWTView(false)
                    setEditMode(true);
                };

                return (
                    <IconButton aria-label="Editar" onClick={onClick}>
                        <Edit />
                    </IconButton>)
            }
        },
        {
            field: "delete",
            headerName: "Eliminar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    DeleteJWT(params.row.id)
                };

                return (
                    <IconButton aria-label="Eliminar" onClick={onClick}>
                        <Delete />
                    </IconButton>)
            }
        },
        {
            field: "showDetails",
            headerName: "Ver detalles",
            sortable: false,
            width: 100,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setJWTData(params.row);
                    setEditMode("show");
                    setAddedJWTView(false);
                };

                return (
                    <IconButton aria-label="Mostrar detalles" onClick={onClick}>
                        <Visibility />
                    </IconButton>)
            }
        }
    ];

    function AddingJWT() {
        var values = {
            icon: "",
            title: "",
            text: ""
        }
        if (!JWTData.enviroments.prodInt && !JWTData.enviroments.prodExt && !JWTData.enviroments.prodIntOn && !JWTData.enviroments.prodExtOn) {
            values.icon = "error";
            values.title = "Error";
            values.text = "No fue seleccionado ningún ambiente";
        }
        else {
            var newData = [...data]
            newData.push({ id: data.length + 1, ...JWTData })
            values.icon = "success";
            values.title = "Agregado";
            values.text = "JWT agregado";
            setData(newData)
            ResetValues();
        }
        Toast.fire({
            ...values,
            target: document.getElementById('generalContainer'),
        })
    }

    function UpdateJWT() {
        var newData = [...data];
        var apiIndex = data.findIndex(envConf => envConf.id === JWTData.id)
        newData[apiIndex] = JWTData;
        setData(newData);
        Toast.fire({
            title: "Actualización",
            text: "Actualización correcta",
            icon: "success",
            target: document.getElementById('generalContainer')
        })
    }

    function DeleteJWT(id) {
        const index = data.findIndex(jwt=> jwt.id === id);
        var newData = [...data];
        newData.splice(index - 1, 1);
        setData(newData);
    }

    function CustomToolbar() {
        return (
            <GridToolbarContainer>
                <GridToolbarColumnsButton />
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
                <GridToolbarExport />
            </GridToolbarContainer>
        );
    }

    function ResetValues() {
        setJWTData({
            appName: "",
            apiName: "",
            enviroments: {
                prodInt: false,
                prodExt: false,
                prodIntOn: false,
                prodExtOn: false
            },
            curls: {
                firstCurl: "",
                secondCurl: "",
                thirdCurl: ""
            },
            details: ""
        });
        setAddedJWTView(false);
        setEditMode(false);
    }

    const ViewJWTComponent = (
        <div className='w-100' style={{ height: 350 }}>
            <Button
                className="mb-2"
                color='primary'
                variant='contained'
                onClick={e => { ResetValues() }}>Agregar JWT</Button>
            <Button
                className="ml-5 mb-2"
                color='success'
                variant='contained'
                onClick={e => {
                    Swal.fire({
                        title: '¿Estás seguro que deseas confirmar los cambios generados?',
                        text: "Se guardarán los JWT modificados",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: 'green',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        target: document.getElementById('sweetContainer')
                    }).then((result) => {
                        if (result.isConfirmed) {
                            SaveJWT();
                        }
                    })
                    setAddedJWTView(true);
                    setEditMode(false)
                }}>Confirmar</Button>
            <DataGrid
                localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                rows={data}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                components={{
                    Toolbar: CustomToolbar
                }}
            />
        </div>
    )

    function SaveJWT() {
        var dataJWT = {
            id: props.DeployDetalis._id,
            Specifications: {
                JWT: []
            }
        }
        dataJWT.Specifications.JWT = data;

        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/setSpec/",
            data: dataJWT,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'JWT' + (dataJWT.Specifications.JWT.length > 1 ? 's' : '') + ' modificado' + (dataJWT.Specifications.JWT.length > 1 ? 's' : ''),
                    target: document.getElementById("generalContainer")
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    const FormJWT = (
        <form onSubmit={e => { e.preventDefault(); editMode ? UpdateJWT() : AddingJWT(); }}>
            <div className="form-group w-100">
                <div className="row">
                    <div className="col-6">
                        <FormLabel>Nombre del APP</FormLabel>
                        <input
                            type="text"
                            value={JWTData.appName}
                            onChange={e => setJWTData(prevState => {
                                let jwt = Object.assign({}, JWTData);
                                jwt.appName = e.target.value
                                return jwt
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Nombre del APP"
                            disabled={editMode === "show"}
                            required
                        />
                        <FormLabel className="mt-2">Nombre del API</FormLabel>
                        <input
                            type="text"
                            value={JWTData.apiName}
                            onChange={e => setJWTData(prevState => {
                                let jwt = Object.assign({}, JWTData);
                                jwt.apiName = e.target.value
                                return jwt
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Nombre del API"
                            disabled={editMode === "show"}
                            required
                        />
                        <div className='row ml-1'>
                            <FormLabel className='w-100 mt-4'>Ambientes</FormLabel>
                            <FormControlLabel
                                control={<Checkbox checked={JWTData.enviroments.prodInt}
                                    disabled={editMode === "show"}
                                    onChange={e => setJWTData(prevState => {
                                        let jwt = Object.assign({}, JWTData);
                                        jwt.enviroments.prodInt = e.target.checked
                                        return jwt
                                    })} name="Prod-Int" />}
                                label="Prod-Int"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={JWTData.enviroments.prodExt}
                                    disabled={editMode === "show"}
                                    onChange={e => setJWTData(prevState => {
                                        let jwt = Object.assign({}, JWTData);
                                        jwt.enviroments.prodExt = e.target.checked
                                        return jwt
                                    })} name="Prod-Ext" />}
                                label="Prod-Ext"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={JWTData.enviroments.prodExtOn}
                                    disabled={editMode === "show"}
                                    onChange={e => setJWTData(prevState => {
                                        let jwt = Object.assign({}, JWTData);
                                        jwt.enviroments.prodExtOn = e.target.checked
                                        return jwt
                                    })} name="Prod-Int-On" />}
                                label="Prod-Int-On"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={JWTData.enviroments.prodIntOn}
                                    disabled={editMode === "show"}
                                    onChange={e => setJWTData(prevState => {
                                        let jwt = Object.assign({}, JWTData);
                                        jwt.enviroments.prodIntOn = e.target.checked
                                        return jwt
                                    })} name="Prod-Ext-On" />}
                                label="Prod-Ext-On"
                            />
                        </div>
                    </div>
                    <div className="col-6">
                        <FormLabel>Primer CURL</FormLabel>
                        <input
                            type="text"
                            value={JWTData.curls.firstCurl}
                            onChange={e => setJWTData(prevState => {
                                let jwt = Object.assign({}, JWTData);
                                jwt.curls.firstCurl = e.target.value;
                                return jwt;
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Primer CURL"
                            disabled={editMode === "show"}
                            required
                        />
                        <FormLabel className="mt-2">Segundo CURL</FormLabel>
                        <input
                            type="text"
                            value={JWTData.curls.secondCurl}
                            onChange={e => setJWTData(prevState => {
                                let jwt = Object.assign({}, JWTData);
                                jwt.curls.secondCurl = e.target.value;
                                return jwt;
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Segundo CURL"
                            disabled={editMode === "show"}
                            required
                        />
                        <FormLabel className="mt-2">Tercer CURL</FormLabel>
                        <input
                            type="text"
                            value={JWTData.curls.thirdCurl}
                            onChange={e => setJWTData(prevState => {
                                let jwt = Object.assign({}, JWTData);
                                jwt.curls.thirdCurl = e.target.value;
                                return jwt;
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Tercer CURL"
                            disabled={editMode === "show"}
                            required
                        />
                        <span>
                            {editMode === "show" ? [] : <input className='btn btn-primary col-2' type="submit" value={!editMode ? "Agregar" : "Actualizar"} />}
                            {
                                editMode && editMode !== "show" ?
                                    <Button
                                        className="ml-5"
                                        color='error'
                                        variant='contained'
                                        onClick={e => { ResetValues(); setAddedJWTView(true) }}>Cancelar</Button> :
                                    <Button
                                        className="ml-5"
                                        color='secondary'
                                        variant='contained'
                                        onClick={e => { setAddedJWTView(true); setEditMode(false) }}>Ver resumen</Button>
                            }
                        </span>
                    </div>
                </div>
            </div>
            <div>
                <FormLabel>Detalles</FormLabel>
                <TextareaAutosize
                    minRows={10}
                    className="w-100"
                    aria-label="Detalles"
                    value={JWTData.details}
                    onChange={e => setJWTData(prevState => {
                        let jwt = Object.assign({}, JWTData);
                        jwt.details = e.target.value
                        return jwt
                    })}
                    disabled={editMode === "show"}
                    placeholder={"Detalles"}
                />
            </div>
        </form>
    );

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 750,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    useEffect(() => {
        if (props.SelectedJWT) {
            setJWTData(props.SelectedJWT);
            setData(props.CurrentJWT);
            setEditMode(true);
            setAddedJWTView(false);
        }
    }, [])

    return (
        <div id="generalContainer" style={
            {
                overflowX: 'hidden',
                overflowY: 'auto',
                maxHeight: '600px'
            }
        }>
            {AddedJWTView ? ViewJWTComponent : FormJWT}
            <div id="sweetContainer" style={{
                zIndex: 300000
            }}></div>
        </div>
    );
}
export default JWT;