import React, { useRef, useState, useEffect } from 'react';
import Settings from '../../Utils/configsettings';

//Axios
import axios from 'axios';

//MUI
import { Button } from '@mui/material';
import { Delete, Save } from '@mui/icons-material';

//Sweet alert 2
import Swal from 'sweetalert2';

function RevreseStrategy(props) {
    const txtReverseRef = useRef();
    const [deletedSelected, setDeletedSelected] = useState(false);

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    function CheckData() {
        var reverseValue = txtReverseRef.current.value;
        if (!reverseValue) {
            Toast.fire({
                icon: 'error',
                title: 'Por favor ingrese un valor',
                target: document.getElementById("ReverseDiv")
            })
            return;
        }
        SaveReverseStrategy(reverseValue);
    }

    function SaveReverseStrategy(reverseStrategy) {
        var deploy = props.deployDetails;
        deploy.id = deploy._id;
        deploy.reverseStrategy = reverseStrategy
        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/update",
            data: deploy,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error,
                    target: document.getElementById("ReverseDiv")
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'Plan de reverso actualizado',
                    target: document.getElementById("ReverseDiv")
                });
                
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al actualizar el despliegue (' + error.message + ')',
                target: document.getElementById("ReverseDiv")
            })
        });
    }

    function DeleteReverseStrategy() {
        var deploy = { ...props.deployDetails };
        deploy.reverseStrategy = "";
        deploy.id = props.deployDetails._id;
        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/update",
            data: deploy,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error,
                    target: document.getElementById("ReverseDiv")
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'Plan de reverso eliminado',
                    target: document.getElementById("ReverseDiv")
                });

            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al actualizar el despliegue (' + error.message + ')',
                target: document.getElementById("ReverseDiv")
            })
        });
    }

    useEffect(() => {
        if (props.currentRS) {
            txtReverseRef.current.value = props.currentRS
        }
    }, [])

    return (
        <div id="ReverseDiv">
            <div className="row mb-3">
                <div className="col-3">
                    <label>Ingresar plan de reverso</label>
                </div>
                <div className="col-2">
                    <Button
                        className="ml-3"
                        color="success"
                        variant="contained"
                        startIcon={<Save />}
                        onClick={e => CheckData()}
                        disabled={deletedSelected}
                    >
                        Guardar
                    </Button>
                </div>
                <div className='col-2'>
                    <Button
                        className="ml-3"
                        color="error"
                        variant="contained"
                        startIcon={<Delete />}
                        onClick={e => Swal.fire({
                            title: '¿Estás seguro que deseas eliminar el plan de reverso?',
                            text: "Se eliminara el plan de reverso guardado previamente",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#d32f2f',
                            cancelButtonColor: '#2e7d32',
                            confirmButtonText: 'Eliminar',
                            cancelButtonText: 'Cancelar',
                            target: document.getElementById('ReverseDiv')
                        }).then((result) => {
                            if (result.isConfirmed) {
                                DeleteReverseStrategy();
                                txtReverseRef.current.disabled = true;
                                txtReverseRef.current.value = "";
                                setDeletedSelected(true)
                            }
                        })}
                        disabled={deletedSelected}>
                        Eliminar
                    </Button>
                </div>
            </div>
            <textarea
                ref={txtReverseRef}
                className="form-control"
                rows="20"
                placeholder="Plan de reverso"
            ></textarea>
        </div>
    )
}
export default RevreseStrategy;