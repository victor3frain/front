import React, { useRef, useState } from 'react';
import Utils from '../../Utils/Utils.js';

//Settings
import Settings from '../../Utils/configsettings.js';

//Sweet alert 2
import Swal from 'sweetalert2';

//MUI
import {
    FormLabel, FormControlLabel, RadioGroup, Radio, Button
} from '@mui/material';

//Axios
import axios from 'axios';

function CURLs(props) {
    const iframe = useRef();
    const collectionRef = useRef();
    const [showResult, setShowResult] = useState(false);
    const [Enviroment, setEnviroment] = useState("1");
    const [Collection, setCollection] = useState(null);
    const devPath = {
        raw: "https://dev-api.bancoazteca.com.mx",
        sub: "dev-api",
        dom: "bancoazteca",
        suf1: "com",
        suf2: "mx"
    }
    const uatPath = {
        raw: "https://qa-api.bancoazteca.com.mx",
        sub: "qa-api",
        dom: "bancoazteca",
        suf1: "com",
        suf2: "mx"
    }
    const uatOnPremisePath = {
        raw: "https://qa-api.bancoazteca.com.mx",
        sub: "qa-api",
        dom: "bancoazteca",
        suf1: "com",
        suf2: "mx"
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    function writeToIframe(Content) {
        iframe.current.contentWindow.document.open();
        iframe.current.contentWindow.document.write(Content);
        iframe.current.contentWindow.document.close();
    }

    function onChange(event) {
        var reader = new FileReader();
        reader.onload = onReaderLoad;
        reader.readAsText(event.target.files[0]);
    }

    function onReaderLoad(event) {
        setShowResult(false);
        var obj = JSON.parse(event.target.result);
        setCollection(obj);
    }

    function onSubmit() {
        if (!Collection) {
            Toast.fire({
                title: 'Por favor ingresa una colección.',
                icon: 'error', target: document.getElementById("curlsDiv")
            });
            return;
        }
        SetEnviromentData();
        TestCollection();
    }

    function SetEnviromentData() {
        var envData;
        switch (Enviroment) {
            case "1":
                envData = devPath;
                break;
            case "2":
                envData = uatPath;
                break;
            case "3":
                envData = uatOnPremisePath
                break;
            default: break;
        }

        for (var urlObject of Collection.item) {
            const index = Utils.getCharPosition(urlObject.request.url.raw, ':', 1);
            urlObject.request.url.raw = envData.raw + urlObject.request.url.raw.substring(index);
            urlObject.request.url.host = [envData.sub, envData.dom, envData.suf1, envData.suf2];
        }
    }

    function TestCollection() {
        axios({
            method: 'post',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/testcollection/",
            data: Collection,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                var content = data.body;
                var result = content.success ? content.success : content.error;
                writeToIframe(result);
                setShowResult(true);
                Toast.fire({
                    icon: 'success',
                    title: 'Prueba exitosa',
                    target: document.getElementById("curlsDiv")
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')',
                target: document.getElementById("curlsDiv")
            })
        });
    }

    return (
        <div id="curlsDiv">
            <div className="form-group row">
                <div className="col-3">
                    <div className='Ambiente'>
                        <FormLabel>Ambiente</FormLabel>
                        <RadioGroup
                            row
                            aria-labelledby="demo-row-radio-buttons-group-label"
                            name="row-radio-buttons-group"
                        >
                            <FormControlLabel
                                value="1"
                                control={<Radio />}
                                label="Desarrollo"
                                checked={Enviroment === "1"}
                                onClick={e => { setEnviroment(e.target.value) }}
                            />
                            <FormControlLabel
                                value="2"
                                control={<Radio />}
                                label="QA"
                                checked={Enviroment === "2"}
                                onClick={e => { setEnviroment(e.target.value) }}
                            />

                            <FormControlLabel
                                value="3"
                                control={<Radio />}
                                label="QA On-Premise"
                                checked={Enviroment === "3"}
                                onClick={e => { setEnviroment(e.target.value) }}
                            />
                        </RadioGroup>
                    </div>
                </div>
                <div className="col-7 mt-1">
                    <label>Colección</label>
                    <div className="input-group">
                        <div className="custom-file">
                            <input
                                type="file"
                                className="custom-file-input"
                                title="Colección"
                                onChange={e => {
                                    onChange(e);
                                    const index = Utils.getCharPosition(e.target.value, '\\', 2);
                                    collectionRef.current.innerHTML = e.target.value.substring(index + 1)
                                }}
                            />
                            <label ref={collectionRef} className="custom-file-label"></label>
                        </div>
                        <Button color='primary' className='ml-5' variant="contained" onClick={e => onSubmit()}>Probar</Button>
                    </div>
                </div>
            </div>
            <div className='card'>
                <div className="card-header">
                    <div className="card-body">
                        <div style={
                            {
                                overflowX: 'hidden',
                                overflowY: 'auto',
                                maxHeight: '600px'
                            }
                        }>

                            <iframe ref={iframe} title="Template de OT" className={'w-100 ' + (!showResult ? 'd-none' : 'd-block')} style={{ height: "600px" }}></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default CURLs;