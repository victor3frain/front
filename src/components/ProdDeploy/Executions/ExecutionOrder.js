import React, { useState } from 'react';

//MUI
import { Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { ContentCopy, Download } from '@mui/icons-material';

//Sweet alert 2
import Swal from 'sweetalert2';


const ExecutionOrder = (props) => {

    const contentOrder = props.Content;
    const [copyLoading, setCopyLoading] = useState(false);
    const [downloadLoading, setDownloadLoading] = useState(false);

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    function download() {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(contentOrder));
        element.setAttribute('download', 'Orden_Ejecucion.txt');

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    return (
        <div className='card'>
            <div className='card-header'>
                <Stack direction="row"
                    justifyContent="flex-end" spacing={2}>
                    <LoadingButton
                        loading={copyLoading}
                        loadingPosition="start"
                        onClick={e => {
                            setCopyLoading(true);
                            navigator.clipboard.writeText(contentOrder);
                            setCopyLoading(false);
                            Toast.fire({
                                icon: "success",
                                title: "Listo",
                                text: "Contenido copiado",
                                target: document.getElementById("content-order")
                            });
                        }}
                        startIcon={<ContentCopy />}
                        variant="outlined"
                    >
                        Copiar
                    </LoadingButton>
                    <LoadingButton
                        loading={downloadLoading}
                        loadingPosition="start"
                        onClick={e => {
                            setDownloadLoading(true);
                            download();
                            Toast.fire({
                                icon: "success",
                                title: "Listo",
                                text: "Descarga lista",
                                target: document.getElementById("content-order")
                            });
                            setDownloadLoading(false);
                        }}
                        startIcon={<Download />}
                        variant="outlined"
                    >
                        Descargar
                    </LoadingButton>
                </Stack>
            </div>
            <div id="content-order" className="card-body" style={
                {
                    overflowX: 'hidden',
                    overflowY: 'auto',
                    maxHeight: '600px',
                    width: '100%'
                }
            }>
                <div dangerouslySetInnerHTML={{ __html: contentOrder.replaceAll("\n", "<br />") }} />
            </div >
        </div>
    );
}
export default ExecutionOrder;