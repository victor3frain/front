import React, { useState, useEffect } from 'react';

//Settings
import Settings from '../../Utils/configsettings.js';

//Axios
import axios from 'axios';

//MUI
import {
    Divider, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio, IconButton, Button
} from '@mui/material';
import {
    DataGrid, esES, GridToolbarContainer, GridToolbarColumnsButton,
    GridToolbarFilterButton, GridToolbarExport, GridToolbarDensitySelector
} from '@mui/x-data-grid';
import { Delete, Edit, Visibility } from '@mui/icons-material';

//Sweet alert 2
import Swal from 'sweetalert2';

function TLS(props) {
    const [nameTLSValue, setNameTLSValue] = useState("");
    const [RadioType, setRadioType] = useState("1");
    const [RadioEnviroment, setRadioEnviroment] = useState("a1");
    const [notes, setNotes] = useState("");
    const [id, setId] = useState(null);
    const [CURLs, SetCURLs] = useState({
        DeletingKS: "",
        DeletingReference: "",
        AddingFirstCurl: "",
        AddingSecondCurl: "",
        AddingThirdCurl: ""
    });
    const [data, setData] = useState([]);
    const [editMode, setEditMode] = useState(false);

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 750,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'Reference',
            headerName: 'Referencia',
            width: 150,
            type: "string",
            description: "Nombre de la referencia",
            valueGetter: (params) => {
                return params.row.targetName
            }
        },
        {
            field: 'keystore',
            headerName: 'Key Store',
            width: 150,
            type: "string",
            sortable: false,
            description: "Nombre del key store",
            valueGetter: (params) => {
                return `ks-${params.row.targetName}`
            }
        },
        {
            field: 'type',
            headerName: 'Tipo',
            width: 150,
            type: "string",
            description: "Tipo de TLS",
            valueGetter: (params) => {
                return params.row.type === 1 ? "Creación" : "Actualización"
            }
        },
        {
            field: "edit",
            headerName: "Editar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setDataDetails(params.row)
                    setEditMode(true);
                };

                return (
                    <IconButton aria-label="Editar" onClick={onClick}>
                        <Edit />
                    </IconButton>)
            }
        },
        {
            field: "delete",
            headerName: "Eliminar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setId(params.row.id)
                    DeleteTLS()
                    ResetValues();
                };

                return (
                    <IconButton aria-label="Eliminar" onClick={onClick}>
                        <Delete />
                    </IconButton>)
            }
        },
        {
            field: "showDetails",
            headerName: "Ver detalles",
            sortable: false,
            width: 100,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setDataDetails(params.row);
                    setEditMode("show");
                };

                return (
                    <IconButton aria-label="Mostrar detalles" onClick={onClick}>
                        <Visibility />
                    </IconButton>)
            }
        }
    ];

    function ResetValues() {
        setRadioType("1");
        setRadioEnviroment("a1")
        setNameTLSValue("")
        SetCURLs({
            DeletingKS: "",
            DeletingReference: "",
            AddingFirstCurl: "",
            AddingSecondCurl: "",
            AddingThirdCurl: ""
        });
        setId(null);
        setEditMode(false);
        setNotes("")
    }

    const creationCurls = (
        <div className='form-group'>
            <FormLabel>Creación de TLS</FormLabel>
            <label>Curls de creación de certificados</label>
        </div>
    )

    function CustomToolbar() {
        return (
            <GridToolbarContainer>
                <GridToolbarColumnsButton />
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
                <GridToolbarExport />
            </GridToolbarContainer>
        );
    }

    const updatingCurls = (
        <div className="form-group">
            <FormLabel>Actualización de certificados</FormLabel>
            <label>CURL de eliminación de Referencia</label>
            <input
                type="text"
                value={CURLs.DeletingReference}
                onChange={e => SetCURLs({
                    DeletingKS: CURLs.DeletingKS,
                    DeletingReference: e.target.value,
                    AddingFirstCurl: CURLs.AddingFirstCurl,
                    AddingSecondCurl: CURLs.AddingSecondCurl,
                    AddingThirdCurl: CURLs.AddingThirdCurl
                })}
                className="form-control form-control-border mb-4"
                placeholder="CURL Referencia"
                required
            />
            <label>CURL de eliminación de Key Store</label>
            <input
                type="text"
                value={CURLs.DeletingKS}
                onChange={e => SetCURLs({
                    DeletingKS: e.target.value,
                    DeletingReference: CURLs.DeletingReference,
                    AddingFirstCurl: CURLs.AddingFirstCurl,
                    AddingSecondCurl: CURLs.AddingSecondCurl,
                    AddingThirdCurl: CURLs.AddingThirdCurl
                })}
                className="form-control form-control-border mb-4"
                placeholder="CURL KS"
                required
            />
            <label>Curls de actualización de certificados</label>
        </div>
    )

    function AddingCurlTemporally(event) {
        event.preventDefault();
        if (editMode === "show") {
            ResetValues();
            return;
        }
        const TLS = {
            targetName: nameTLSValue,
            type: parseInt(RadioType),
            enviroment: RadioEnviroment,
            notes: notes,
            CURLs: CURLs
        }
        var newData = [...data];
        if (editMode) {
            var dataIndex = newData.findIndex(tls => tls.id === id)
            TLS.id = id;
            newData[dataIndex] = TLS;
        }
        else {
            TLS.id = data.length + 1
            newData.push(TLS);
        }

        setData(newData);
        setEditMode(false);
        ResetValues();
    }

    function DeleteTLS() {
        var newData = [...data];
        const TLSIndex = newData.findIndex(tls => tls.id === id);
        newData.splice(TLSIndex, 1);
        setData(newData);
    }

    function setDataDetails(row) {
        setId(row.id);
        SetCURLs(row.CURLs)
        setRadioType(row.type.toString())
        setNameTLSValue(row.targetName)
        setRadioEnviroment(row.enviroment)
        setNotes(row.notes)
    }

    function SaveTLS() {
        var dataTLS = {
            id: props.DeployDetalis._id,
            Specifications: {
                TLS: data
            }
        }
        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/setSpec/",
            data: dataTLS,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'TLS' + (dataTLS.Specifications.TLS.length > 1 ? 's' : '') + ' modificado' + (dataTLS.Specifications.TLS.length > 1 ? 's' : ''),
                    target: document.getElementById("TLSModal")
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    useEffect(() => {
        if (props.SelectedTLS) {
            setData(props.CurrentTLS);
            setDataDetails(props.SelectedTLS)
            setEditMode(true);
        }
    }, [])

    return (
        <div id="TLSModal" style={
            {
                overflowX: 'hidden',
                overflowY: 'auto',
                maxHeight: '600px'
            }
        }>
            <form onSubmit={e => AddingCurlTemporally(e)}>
                <div className="form-group w-100">
                    <div className="row">
                        <div className="col-4">
                            <FormLabel>Nombre del Target Server</FormLabel>
                            <input
                                type="text"
                                value={nameTLSValue}
                                onChange={e => setNameTLSValue(e.target.value)}
                                className="form-control form-control-border mb-2"
                                placeholder="Nombre"
                                disabled={editMode === "show"}
                                required
                            />
                            <a
                                className="ml-1"
                                href={"https://10.63.32.231/certificados"}
                                target="_BLANK"
                                rel="noopener noreferrer">
                                Listado de tls
                            </a>
                            <div className="form-group mt-2">
                                <label>Notas</label>
                                <textarea
                                    disabled={editMode === "show"}
                                    value={notes}
                                    className="form-control"
                                    rows="4"
                                    onChange={e => setNotes(e.target.value)} />
                            </div>
                            <input
                                className={"btn " + (editMode ? "btn-success" : "btn-primary") + " d-flex mt-5"}
                                type="submit"
                                value={editMode === "show" ? "Regresar" : ((!editMode ? "Agregar" : "Guardar") + " TLS")} />
                            <Button
                                className='mt-2'
                                color={editMode ? "error" : "success"}
                                variant="contained"
                                disabled={editMode === "show"}
                                onClick={e => {
                                    if (editMode) {
                                        ResetValues();
                                        return;
                                    }
                                    Swal.fire({
                                        title: '¿Estás seguro que deseas confirmar los cambios generados?',
                                        text: "Se guardaran los TLS(s) modificados",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: 'green',
                                        cancelButtonColor: '#3085d6',
                                        confirmButtonText: 'Confirmar',
                                        cancelButtonText: 'Cancelar',
                                        target: document.getElementById('sweetContainer')
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            SaveTLS();
                                        }
                                    })
                                }}
                            > {editMode ? "Cancelar" : "Confirmar"}</Button>
                        </div>
                        <Divider orientation="vertical" flexItem />
                        <div className="col-3">
                            <FormControl>
                                <FormLabel>Tipo de ejecución</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="demo-row-radio-buttons-group-label"
                                    name="row-radio-buttons-group"
                                >
                                    <FormControlLabel
                                        value="1"
                                        control={<Radio />}
                                        label="Creación"
                                        checked={RadioType === "1"}
                                        onClick={e => {
                                            if (editMode === "show")
                                                return;
                                            setRadioType(e.target.value)
                                        }}
                                        disabled={editMode === "show"}
                                    />
                                    <FormControlLabel
                                        value="2"
                                        control={<Radio />}
                                        label="Actualización"
                                        checked={RadioType === "2"}
                                        onClick={e => {
                                            if (editMode === "show")
                                                return;
                                            setRadioType(e.target.value)
                                        }}
                                        disabled={editMode === "show"}
                                    />
                                </RadioGroup>
                            </FormControl>
                            <FormControl>
                                <FormLabel>Ambientes</FormLabel>
                                <FormControlLabel
                                    value="a1"
                                    control={<Radio />}
                                    label="Prod-Int"
                                    checked={RadioEnviroment === "a1"}
                                    onClick={e => {
                                        if (editMode === "show")
                                            return;
                                        setRadioEnviroment(e.target.value)
                                    }}
                                    disabled={editMode === "show"}
                                />
                                <FormControlLabel
                                    value="a2"
                                    control={<Radio />}
                                    label="Prod-Ext"
                                    checked={RadioEnviroment === "a2"}
                                    onClick={e => {
                                        if (editMode === "show")
                                            return;
                                        setRadioEnviroment(e.target.value)
                                    }}
                                    disabled={editMode === "show"}
                                />

                                <FormControlLabel
                                    value="a3"
                                    control={<Radio />}
                                    label="Prod-Int-OnPremise"
                                    checked={RadioEnviroment === "a3"}
                                    onClick={e => {
                                        if (editMode === "show")
                                            return;
                                        setRadioEnviroment(e.target.value)
                                    }}
                                    disabled={editMode === "show"}
                                />

                                <FormControlLabel
                                    value="a4"
                                    control={<Radio />}
                                    label="Prod-Ext-OnPremise"
                                    checked={RadioEnviroment === "a4"}
                                    onClick={e => {
                                        if (editMode === "show")
                                            return;
                                        setRadioEnviroment(e.target.value)
                                    }}
                                    disabled={editMode === "show"}
                                />
                            </FormControl>
                        </div>
                        <Divider orientation="vertical" flexItem />
                        <div className='col-4'>
                            {
                                RadioType === "1" ?
                                    creationCurls
                                    : updatingCurls
                            }
                            <input
                                type="text"
                                value={CURLs.AddingFirstCurl}
                                onChange={e => SetCURLs({
                                    DeletingKS: CURLs.DeletingKS,
                                    DeletingReference: CURLs.DeletingReference,
                                    AddingFirstCurl: e.target.value,
                                    AddingSecondCurl: CURLs.AddingSecondCurl,
                                    AddingThirdCurl: CURLs.AddingThirdCurl
                                })}
                                className="form-control form-control-border mb-4"
                                placeholder="CURL Key Store"
                                disabled={editMode === "show"}
                            />
                            <input
                                type="text"
                                value={CURLs.AddingSecondCurl}
                                onChange={e => {
                                    SetCURLs({
                                        DeletingKS: CURLs.DeletingKS,
                                        DeletingReference: CURLs.DeletingReference,
                                        AddingFirstCurl: CURLs.AddingFirstCurl,
                                        AddingSecondCurl: e.target.value,
                                        AddingThirdCurl: CURLs.AddingThirdCurl
                                    })
                                }}
                                className="form-control form-control-border mb-4"
                                placeholder="CURL Archivo p12"
                                disabled={editMode === "show"}
                            />
                            <input
                                type="text"
                                value={CURLs.AddingThirdCurl}
                                onChange={e => SetCURLs({
                                    DeletingKS: CURLs.DeletingKS,
                                    DeletingReference: CURLs.DeletingReference,
                                    AddingFirstCurl: CURLs.AddingFirstCurl,
                                    AddingSecondCurl: CURLs.AddingSecondCurl,
                                    AddingThirdCurl: e.target.value
                                })}
                                className="form-control form-control-border mb-4"
                                placeholder="CURLS Referencia"
                                disabled={editMode === "show"}
                            />
                        </div>
                    </div>
                </div>
            </form >
            <div style={{ height: data.length === 0 ? 200 : 400, width: '100%' }}>
                <DataGrid
                    localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                    rows={data}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    components={{
                        Toolbar: CustomToolbar
                    }}
                />
            </div>
            <div id="sweetContainer" style={{
                zIndex: 300000
            }}></div>
        </div >
    );
}
export default TLS;