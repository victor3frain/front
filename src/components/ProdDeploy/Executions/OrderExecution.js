import React, { useState, useRef, useEffect } from 'react';
import Settings from '../../Utils/configsettings';

//Material
import { TextField, Button } from '@mui/material';

//Axios
import axios from 'axios';

//Sweet alert 2
import Swal from 'sweetalert2';

const OrderExecution = () => {
    const [OrderDateValue, setOrderDateValue] = useState(new Date().toISOString().split('T')[0]);
    const [OrderExecution, setOrderExecution] = useState("")
    const iframe = useRef();

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    function writeToIframe() {
        iframe.current.contentWindow.document.open();
        iframe.current.contentWindow.document.write(OrderExecution);
        iframe.current.contentWindow.document.close();
    }

    function GetOrderExecution() {
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/getExecutionOrder/" + OrderDateValue,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error,
                    target: document.getElementById("modal-execution")
                });
            }
            else {
                setOrderExecution(data.body);
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener la orden de ejecución (' + error.message + ')',
                target: document.getElementById("modal-execution")
            })
        });
    }

    useEffect(() => {
        writeToIframe()
    }, [OrderExecution])

    return (
        <span id='modal-execution'>
            <div className='card d-inline-block'>
                <div className='card-body'>
                    <TextField
                        label="Fecha de despliegue"
                        type="date"
                        value={OrderDateValue}
                        sx={{ width: 220 }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={e => { setOrderDateValue(e.target.value) }}
                    />
                    <Button
                        className="ml-3 mt-2"
                        variant="contained"
                        color="success"
                        onClick={e => {
                            if (!OrderDateValue) {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Fecha incorrecta',
                                    target: document.getElementById("modal-execution")
                                });
                                return;
                            }
                            GetOrderExecution();
                        }}>Generar
                    </Button>
                </div>
            </div>
            <div className='ml-5 d-inline-block' style={{ width: "70%" }}>
                <div className='card'>
                    <div className='card-header'>
                        <h3 className='card-title'>Previo de orden</h3>
                    </div>
                    <div className='card-body'>
                        <iframe ref={iframe} title="Template de OT" className='w-100' style={{ height: "500px" }}></iframe>
                    </div>
                </div>
            </div>
        </span>
    );
}
export default OrderExecution;