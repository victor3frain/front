import React, { useState, useEffect } from 'react';

//Settings
import Settings from '../../Utils/configsettings.js';

//Axios
import axios from 'axios';

//MUI
import {
    FormControl, FormLabel, Checkbox, FormControlLabel, TextareaAutosize, IconButton, Button, Radio, RadioGroup
} from '@mui/material';
import {
    DataGrid, esES, GridToolbarContainer, GridToolbarColumnsButton,
    GridToolbarFilterButton, GridToolbarExport, GridToolbarDensitySelector
} from '@mui/x-data-grid';
import { Delete, Edit, Visibility } from '@mui/icons-material';

//Sweet alert 2
import Swal from 'sweetalert2';

function API(props) {
    const [APIData, setAPIData] = useState({
        name: "",
        repo: "",
        enviroments: {
            prodInt: false,
            prodExt: false,
            prodIntOn: false,
            prodExtOn: false
        },
        origin: "uat",
        type: "new",
        details: ""
    });
    const [data, setData] = useState([]);
    const [AddedCAView, setAddedAPIView] = useState(true);
    const [editMode, setEditMode] = useState(false);

    function AddingAPI() {
        var values = {
            icon: "",
            title: "",
            text: ""
        }
        if (!APIData.enviroments.prodInt && !APIData.enviroments.prodExt && !APIData.enviroments.prodIntOn && !APIData.enviroments.prodExtOn) {
            values.icon = "error";
            values.title = "Error";
            values.text = "No fue seleccionado ningún API";
        }
        else {
            var newData = [...data]
            newData.push({ id: data.length + 1, ...APIData })
            values.icon = "success";
            values.title = "Agregado";
            values.text = "API agregado";
            setData(newData)
            ResetValues();
        }
        Toast.fire({
            ...values,
            target: document.getElementById('generalContainer'),
        })
    }

    function ResetValues() {
        setAPIData({
            name: "",
            repo: "",
            enviroments: {
                prodInt: false,
                prodExt: false,
                prodIntOn: false,
                prodExtOn: false
            },
            origin: "uat",
            type: "new",
            details: ""
        });
        setAddedAPIView(false);
        setEditMode(false);
    }

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'name',
            headerName: 'Nombre',
            width: 150,
            type: "string",
            sortable: false,
            description: "Nombre del API"
        },
        {
            field: 'repo',
            headerName: 'Link',
            width: 150,
            description: "Link del API",
            renderCell: (params) => {
                return <a href={params.row.repo} target="_BLANK" rel="noopener noreferrer">Link del repo</a>
            }
        },
        {
            field: 'enviroments',
            headerName: 'Ambientes',
            width: 400,
            type: "string",
            description: "Ambientes de desarrollo",
            valueGetter: (params) => {
                var enviroments = [];
                const env = params.row.enviroments;
                if (env.prodInt)
                    enviroments.push("prod-int");
                if (env.prodExt)
                    enviroments.push("prod-ext");
                if (env.prodIntOn)
                    enviroments.push("prod-int-onpremise");
                if (env.prodExtOn)
                    enviroments.push("prod-ext-onpremise");

                var value = "";
                for (let envName of enviroments)
                    value += envName + ", "
                value = value.substring(0, value.length - 2);
                return "[" + value + "]"
            }
        },
        {
            field: "edit",
            headerName: "Editar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setAPIData(params.row)
                    setAddedAPIView(false)
                    setEditMode(true);
                };

                return (
                    <IconButton aria-label="Editar" onClick={onClick}>
                        <Edit />
                    </IconButton>)
            }
        },
        {
            field: "delete",
            headerName: "Eliminar",
            sortable: false,
            width: 70,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    DeleteAPI(params.row.id)
                };

                return (
                    <IconButton aria-label="Eliminar" onClick={onClick}>
                        <Delete />
                    </IconButton>)
            }
        },
        {
            field: "showDetails",
            headerName: "Ver detalles",
            sortable: false,
            width: 100,
            align: "center",
            renderCell: (params) => {
                const onClick = (e) => {
                    setAPIData(params.row);
                    setEditMode("show");
                    setAddedAPIView(false);
                };

                return (
                    <IconButton aria-label="Mostrar detalles" onClick={onClick}>
                        <Visibility />
                    </IconButton>)
            }
        }
    ];

    function CustomToolbar() {
        return (
            <GridToolbarContainer>
                <GridToolbarColumnsButton />
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
                <GridToolbarExport />
            </GridToolbarContainer>
        );
    }

    function UpdateAPI() {
        var newData = [...data];
        var apiIndex = data.findIndex(envConf => envConf.id === APIData.id)
        newData[apiIndex] = APIData;
        setData(newData);
        Toast.fire({
            title: "Actualización",
            text: "Actualización correcta",
            icon: "success",
            target: document.getElementById('generalContainer')
        })
    }

    function DeleteAPI(id) {
        const index = data.findIndex(api => api.id === id);
        var newData = [...data];
        newData.splice(index - 1, 1);
        setData(newData);
    }

    const APIComponent = (
        <form onSubmit={e => { e.preventDefault(); editMode ? UpdateAPI() : AddingAPI(); }}>
            <div className="form-group w-100">
                <div className="row">
                    <div className="col-6">
                        <FormLabel>Nombre del API</FormLabel>
                        <input
                            type="text"
                            value={APIData.name}
                            onChange={e => setAPIData(prevState => {
                                let api = Object.assign({}, APIData);
                                api.name = e.target.value
                                return api
                            }
                            )}
                            className="form-control form-control-border mb-2"
                            placeholder="Nombre"
                            disabled={editMode === "show"}
                            required
                        />
                        <FormLabel>Link del repositorio</FormLabel>
                        <input
                            type="text"
                            value={APIData.repo}
                            onChange={e => setAPIData(prevState => {
                                let api = Object.assign({}, APIData);
                                api.repo = e.target.value
                                return api
                            })}
                            disabled={editMode === "show"}
                            className="form-control form-control-border mb-2"
                            placeholder="Link"
                            required
                        />
                        <div>
                            <FormControl>
                                <FormLabel className='w-100'>Tipo de despliegue</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="demo-row-radio-buttons-group-label"
                                    name="row-radio-buttons-group"
                                >
                                    <FormControlLabel
                                        value="1"
                                        control={<Radio />}
                                        label="Nueva"
                                        checked={APIData.type === "new"}
                                        onClick={e => setAPIData(prevState => {
                                            let api = Object.assign({}, APIData);
                                            api.type = "new"
                                            return api
                                        })}
                                        disabled={editMode === "show"}
                                    />
                                    <FormControlLabel
                                        value="2"
                                        control={<Radio />}
                                        label="Actualización"
                                        checked={APIData.type === "update"}
                                        onClick={e => setAPIData(prevState => {
                                            let api = Object.assign({}, APIData);
                                            api.type = "update"
                                            return api
                                        })}
                                        disabled={editMode === "show"}
                                    />
                                </RadioGroup>
                            </FormControl>
                        </div>
                    </div>
                    <div className="col-6">
                        <FormControl>
                            <FormLabel className='w-100'>Origen</FormLabel>
                            <RadioGroup
                                row
                                aria-labelledby="demo-row-radio-buttons-group-label"
                                name="row-radio-buttons-group"
                            >
                                <FormControlLabel
                                    value="1"
                                    control={<Radio />}
                                    label="AWS"
                                    checked={APIData.origin === "uat"}
                                    onClick={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.origin = "uat"
                                        return api
                                    })}
                                    disabled={editMode === "show"}
                                />
                                <FormControlLabel
                                    value="2"
                                    control={<Radio />}
                                    label="On Premise"
                                    checked={APIData.origin === "uat-onpremise"}
                                    onClick={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.origin = "uat-onpremise"
                                        return api
                                    })}
                                    disabled={editMode === "show"}
                                />
                            </RadioGroup>
                            <FormLabel className='w-100'>Destinos</FormLabel>
                            <div className='row'>
                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={APIData.enviroments.prodInt} disabled={editMode === "show"} onChange={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.enviroments.prodInt = e.target.checked
                                        return api
                                    })} name="Prod-Int" />}
                                    label="Prod-Int"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={APIData.enviroments.prodExt} disabled={editMode === "show"} onChange={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.enviroments.prodExt = e.target.checked
                                        return api
                                    })} name="Prod-Ext" />}
                                    label="Prod-Ext"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={APIData.enviroments.prodIntOn} disabled={editMode === "show"} onChange={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.enviroments.prodIntOn = e.target.checked
                                        return api
                                    })} name="Prod-Int-OnPromise" />}
                                    label="Prod-Int-OnPremise"
                                />

                                <FormControlLabel
                                    className='col-5'
                                    control={<Checkbox checked={APIData.enviroments.prodExtOn} disabled={editMode === "show"} onChange={e => setAPIData(prevState => {
                                        let api = Object.assign({}, APIData);
                                        api.enviroments.prodExtOn = e.target.checked
                                        return api
                                    })} name="Prod-Ext-OnPremise" />}
                                    label="Prod-Ext-OnPremise"
                                />
                            </div>
                            <span>
                                {editMode === "show" ? [] : <input className='btn btn-primary col-2' type="submit" value={!editMode ? "Agregar" : "Actualizar"} />}
                                {
                                    editMode && editMode !== "show" ?
                                        <Button
                                            className="ml-5"
                                            color='error'
                                            variant='contained'
                                            onClick={e => { ResetValues(); setAddedAPIView(true) }}>Cancelar</Button> :
                                        <Button
                                            className="ml-5"
                                            color='secondary'
                                            variant='contained'
                                            onClick={e => { setAddedAPIView(true); setEditMode(false) }}>Ver resumen</Button>
                                }
                            </span>
                        </FormControl>
                    </div>
                </div>
                <div>
                    <FormLabel>Detalles</FormLabel>
                    <TextareaAutosize
                        minRows={10}
                        className="w-100"
                        aria-label="Detalles"
                        value={APIData.details}
                        onChange={e => setAPIData(prevState => {
                            let api = Object.assign({}, APIData);
                            api.details = e.target.value
                            return api
                        })}
                        disabled={editMode === "show"}
                        placeholder={"Detalles"}
                    />
                </div>
            </div>
        </form>
    );

    function SaveAPI() {
        var dataAPI = {
            id: props.DeployDetalis._id,
            Specifications: {
                API: []
            }
        }
        dataAPI.Specifications.API = data;

        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/setSpec/",
            data: dataAPI,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'API' + (dataAPI.Specifications.API.length > 1 ? 's' : '') + ' modificado' + (dataAPI.Specifications.API.length > 1 ? 's' : ''),
                    target: document.getElementById("generalContainer")
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    const AddedCAComponent = (
        <div className='w-100' style={{ height: 350 }}>
            <Button
                className="mb-2"
                color='primary'
                variant='contained'
                onClick={e => { ResetValues() }}>Agregar API</Button>
            <Button
                className="ml-5 mb-2"
                color='success'
                variant='contained'
                onClick={e => {
                    Swal.fire({
                        title: '¿Estás seguro que deseas confirmar los cambios generados?',
                        text: "Se guardarán los APIs modificados",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: 'green',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        target: document.getElementById('sweetContainer')
                    }).then((result) => {
                        if (result.isConfirmed) {
                            SaveAPI();
                        }
                    })
                    setAddedAPIView(true);
                    setEditMode(false)
                }}>Confirmar</Button>
            <DataGrid
                localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                rows={data}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                components={{
                    Toolbar: CustomToolbar
                }}
            />
        </div>
    )

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 750,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    useEffect(() => {
        if (props.SelectedAPI) {
            setAPIData(props.SelectedAPI);
            setData(props.CurrentAPI);
            setEditMode(true);
            setAddedAPIView(false);
        }
    }, [])

    return (
        <div id="generalContainer" style={
            {
                overflowX: 'hidden',
                overflowY: 'auto',
                maxHeight: '600px'
            }
        }>
            {AddedCAView ? AddedCAComponent : APIComponent}
            <div id="sweetContainer" style={{
                zIndex: 300000
            }}></div>
        </div>
    )
}
export default API;