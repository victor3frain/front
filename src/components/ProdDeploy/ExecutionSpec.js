import React, { useEffect, useState } from 'react';

//Settings
import Settings from '../Utils/configsettings.js';

//Executions classes
import ExecutionOrder from './Executions/ExecutionOrder.js';
import TLS from './Executions/TLS';
import CA from './Executions/CA.js';
import API from './Executions/API.js';
import JWT from './Executions/JWT.js';
import ReverseStrategy from './Executions/ReverseStrategy';
import CURLs from './Executions/CURLs.js';

//Axios
import axios from 'axios';

//Sweet alert 2
import Swal from 'sweetalert2';

//MUI
import { TreeView, TreeItem } from '@mui/lab';
import { ExpandMore, ChevronRight, Info, Add, Cancel, Close, Article } from '@mui/icons-material';
import {
    FormControlLabel, Select, MenuItem, OutlinedInput, Backdrop,
    IconButton, Typography, FormLabel, Modal, Fade, Box
} from '@mui/material';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';


const ExecutionSpec = (props) => {
    const [showSpecForm, setShowSpecForm] = useState(false);
    const [executionValue, setExecutionValue] = useState(-1);
    const [EditingForm, setEditingForm] = useState({
        title: "",
        component: [],
        showSpecEditing: false
    });
    const HtmlTooltip = styled(({ className, ...props }) => (
        <Tooltip {...props} classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: '#f5f5f9',
            color: 'rgba(0, 0, 0, 0.87)',
            maxWidth: 220,
            fontSize: theme.typography.pxToRem(12),
            border: '1px solid #dadde9',
        },
    }));
    const [TreeExecutions, setTreeExceutions] = useState([]);
    const [specifications, setSpecifications] = useState(null);

    const settingSpecifications = {
        TLS: (TLSSpec) => {
            return (
                TLSSpec.length === 0 ? [] :
                    <TreeItem nodeId="tls_1" label="TLS">
                        {TLSSpec.map((tlsObject, index) => {
                            return (
                                <TreeItem key={"tls_" + index + 2} nodeId={"tls_" + (index + 2)} label={
                                    <FormControlLabel sx={{ margin: "auto" }} control={
                                        <Info className="ml-2 mb-1" fontSize="small" onClick={e => {
                                            SelectFormComponent(0, tlsObject);
                                        }} />
                                    } label={tlsObject.targetName} labelPlacement="start" />
                                } />
                            )
                        })}
                    </TreeItem >
            )
        },
        JWT: (JWTSpec) => {
            return (
                JWTSpec.length === 0 ? [] :
                    <TreeItem nodeId="jwt_1" label="JWT">
                        {JWTSpec.map((jwtObject, index) => {
                            return (
                                <TreeItem key={"jwt_" + index + 2} nodeId={"jwt_" + (index + 2)} label={
                                    <FormControlLabel sx={{ margin: "auto" }} control={
                                        <Info className="ml-2 mb-1" fontSize="small" onClick={e => {
                                            SelectFormComponent(1, jwtObject);
                                        }} />
                                    } label={jwtObject.appName} labelPlacement="start" />
                                } />
                            )
                        })}
                    </TreeItem >
            )
        },
        API: (APISpec) => {
            return (
                APISpec.length === 0 ? [] :
                    <TreeItem nodeId="api_1" label="API">
                        {APISpec.map((api, index) => {
                            return (
                                <TreeItem key={"api_" + index + 2} nodeId={"api_" + (index + 2)} label={
                                    <FormControlLabel sx={{ margin: "auto" }} control={
                                        <Info className="ml-2 mb-1" fontSize="small" onClick={e => {
                                            SelectFormComponent(4, api);
                                        }} />
                                    } label={api.name} labelPlacement="start" />
                                } />
                            )
                        })}
                    </TreeItem >
            )
        },
        CA: (CASpec, caType, indexId) => {
            return (
                CASpec.length === 0 ? [] :
                    <TreeItem nodeId={"ca_" + indexId} label={"Configurador de ambiente [" + caType + "]"}>
                        {CASpec.map((ca, index) => {
                            return (
                                <TreeItem key={"ca_" + caType + "_" + index + 2} nodeId={"ca_" + caType + "_" + (index + 2)} label={
                                    <FormControlLabel sx={{ margin: "auto" }} control={
                                        <Info className="ml-2 mb-1" fontSize="small" onClick={e => {
                                            SelectFormComponent(indexId, ca);
                                        }} />
                                    } label={ca.name} labelPlacement="start" />
                                } />
                            )
                        })}
                    </TreeItem >
            )
        },
        ReverseStrategy: (RSValue) => {
            return (
                RSValue ?
                    <TreeItem nodeId={"reverseStrategy"} label={
                        <FormControlLabel sx={{ margin: "auto" }} control={
                            <Info className="ml-2 mb-1" fontSize="small" onClick={e => {
                                SelectFormComponent(10);
                            }} />
                        } label={"Plan de reverso"} labelPlacement="start" />
                    }></TreeItem > : []
            )
        }
    }

    function EnviromentConfig({ type, deployDetails, currentCA, selectedCA }) {
        return <CA Type={type} DeployDetalis={deployDetails} CurrentCA={currentCA} SelectedCA={selectedCA} />
    }

    const MemoizedCA = React.memo(EnviromentConfig);

    function APIConfig({ deployDetails, currentAPI, selectedAPI }) {
        return <API DeployDetalis={deployDetails} CurrentAPI={currentAPI} SelectedAPI={selectedAPI} />
    }

    const MemoizedAPI = React.memo(APIConfig);

    function JWTConfig({ deployDetails, currentJWT, selectedJWT }) {
        return <JWT DeployDetalis={deployDetails} CurrentJWT={currentJWT} SelectedJWT={selectedJWT} />
    }

    const MemoizedJWT = React.memo(JWTConfig);

    const SelectFormComponent = (option, selectedOption, settedValue) => {
        var editingComponent = [],
            showSpecEditing = false,
            titleEditingForm = "", nameComponent = "";
        switch (option) {
            case 0:
                editingComponent = (
                    <TLS User={props.User} DeployDetalis={props.DeployDetails} CurrentTLS={specifications.TLS} SelectedTLS={selectedOption} />
                );
                titleEditingForm = "TLS";
                showSpecEditing = true;
                break;
            case 1:
                editingComponent = (<MemoizedJWT deployDetails={props.DeployDetails} currentJWT={specifications.JWT} selectedJWT={selectedOption} />)
                titleEditingForm = "JWT";
                showSpecEditing = true;
                break;
            case 4:
                editingComponent = (
                    <MemoizedAPI deployDetails={props.DeployDetails} currentAPI={specifications.API} selectedAPI={selectedOption} />
                );
                titleEditingForm = "API(s)";
                showSpecEditing = true;
                break;
            case 2:
            case 5:
            case 6:
            case 7:
            case 8:
                switch (option) {
                    case 2:
                        nameComponent = "ambiente";
                        break;
                    case 5:
                        nameComponent = "componentes";
                        break;
                    case 6:
                        nameComponent = "app";
                        break;
                    case 7:
                        nameComponent = "app-key";
                        break;
                    case 8:
                        nameComponent = "producto";
                        break;
                    default: break;
                }
                editingComponent = <MemoizedCA type={nameComponent} deployDetails={props.DeployDetails} currentCA={specifications.CA} selectedCA={selectedOption} />
                titleEditingForm = "Configurador de ambiente [" + nameComponent + "]";
                showSpecEditing = true;
                break;
            case 9:
                editingComponent = <CURLs deployDetails={props.DeployDetails} currentCURLs={specifications.CURLs} />
                titleEditingForm = "CURLs de validación";
                showSpecEditing = true;
                break;
            case 10:
                editingComponent = <ReverseStrategy deployDetails={props.DeployDetails} currentRS={specifications.reverseStrategy} />
                titleEditingForm = "Plan de reverso";
                showSpecEditing = true;
                break;
            case "SAVE":
                alert("Guardado");
                ResetValues();
                break;
            case "ORDEREXECUTION":
                editingComponent = (
                    <ExecutionOrder Content={settedValue} />
                );
                titleEditingForm = "Orden de ejecución";
                showSpecEditing = true;
                break;
            default:
                titleEditingForm = "";
                editingComponent = EditingForm.component
                showSpecEditing = false
        }

        setEditingForm({
            title: titleEditingForm,
            component: editingComponent,
            showSpecEditing: showSpecEditing
        });
    }

    function ResetValues() {
        setShowSpecForm(false)
        setExecutionValue(-1);
    }

    const handleCloseModal = () => {
        setExecutionValue(-1)
        setEditingForm({
            title: "",
            component: EditingForm.component,
            showSpecEditing: false
        });
        GetSpecifications();
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    const GetSpecifications = () => {
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/getspec/" + props.DeployDetails._id,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                setSpecifications({ ...data.body });
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    function updateTreeSpecifications() {
        const specNames = Object.keys(specifications)
        const specComponents = {};
        for (const spec of specNames) {
            switch (spec.toUpperCase()) {
                case "TLS":
                    specComponents.TLS = settingSpecifications.TLS(specifications[spec]);
                    break;
                case "JWT":
                    specComponents.JWT = settingSpecifications.JWT(specifications[spec]);
                    break;
                case "CA":
                    const caNames = Object.keys(specifications.CA)
                    for (const ca of caNames) {
                        switch (ca.toUpperCase()) {
                            case "AMBIENTE":
                                specComponents.CAAMB = settingSpecifications.CA(specifications.CA[ca], ca, 2);
                                break;
                            case "COMPONENTES":
                                specComponents.CACOMP = settingSpecifications.CA(specifications.CA[ca], ca, 5);
                                break;
                            case "APP":
                                specComponents.CAAPP = settingSpecifications.CA(specifications.CA[ca], ca, 6);
                                break;
                            case "APP-KEY":
                                specComponents.CAAPPKEY = settingSpecifications.CA(specifications.CA[ca], ca, 7);
                                break;
                            case "PRODUCTO":
                                specComponents.CAPROD = settingSpecifications.CA(specifications.CA[ca], ca, 8);
                                break;
                            default: break;
                        }
                    }
                    break;
                case "API":
                    specComponents.API = settingSpecifications.API(specifications[spec]);
                    break;
                case "REVERSESTRATEGY":
                    specComponents.ReverseStrategy = settingSpecifications.ReverseStrategy(specifications[spec]);
                    break;
                default: break;
            }
        }
        setTreeExceutions(<TreeView
            aria-label="multi-select"
            defaultCollapseIcon={<ExpandMore />}
            defaultExpandIcon={<ChevronRight />}
            multiSelect
            sx={{ height: !IsThereSpecifications() ? 0 : 216, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
        >
            {specComponents.TLS}
            {specComponents.JWT}
            {specComponents.CAAMB}
            {specComponents.API}
            {specComponents.CACOMP}
            {specComponents.CAAPP}
            {specComponents.CAAPPKEY}
            {specComponents.CAPROD}
            {specComponents.ReverseStrategy}
        </TreeView>)
    }

    function GetSpecDocuments() {
        if (!IsThereSpecifications())
            return;
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/getDocumentSpec/" + props.DeployDetails._id,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                SelectFormComponent("ORDEREXECUTION", null, data.body)
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    const IsThereSpecifications = () => {
        var emptySpec = false;
        if (specifications) {
            if (showSpecForm) {
                emptySpec = true;
            }
            else {
                if ((specifications.TLS instanceof Object && specifications.TLS.length > 0) ||
                    (specifications.API instanceof Object && specifications.API.length > 0) ||
                    (specifications.JWT instanceof Object && specifications.JWT.length > 0) ||
                    ((specifications.CA instanceof Object) && (
                    (specifications.CA.ambiente instanceof Object && specifications.CA.ambiente.length > 0) ||
                        (specifications.CA.componentes instanceof Object && specifications.CA.componentes.length > 0) ||
                        (specifications.CA.app instanceof Object && specifications.CA.app.length > 0) ||
                        (specifications.CA["app-key"] instanceof Object && specifications.CA["app-key"].length > 0) ||
                        (specifications.CA.producto instanceof Object && specifications.CA.producto.length > 0)))) {
                    emptySpec = true;
                }
            }
        }
        return emptySpec;
    }

    useEffect(() => {
        if (specifications) {
            updateTreeSpecifications();
        }
    }, [specifications]);

    useEffect(() => {
        GetSpecifications();
    }, [])

    return (
        <dl style={{ width: "30%" }} className="ml-3">
            <dt>Especificaciones {!showSpecForm ?
                <HtmlTooltip
                    title={
                        <React.Fragment>
                            <Typography color="inherit">{"Agregar ejecución"}</Typography>
                        </React.Fragment>
                    }
                >
                    <IconButton
                        className="ml-3"
                        onClick={e => { setShowSpecForm(true) }}
                    >
                        <Add />
                    </IconButton>
                </HtmlTooltip> : []}
                {showSpecForm ?
                    <HtmlTooltip
                        title={
                            <React.Fragment>
                                <Typography color="inherit">Cancelar</Typography>
                            </React.Fragment>
                        }
                    >
                        <IconButton className="ml-3" onClick={e => {
                            ResetValues()
                            GetSpecifications();
                        }}>
                            <Cancel />
                        </IconButton>
                    </HtmlTooltip> : []}
                {
                    !showSpecForm ?
                        <HtmlTooltip
                            title={
                                <React.Fragment>
                                    <Typography color="inherit">Obtener orden de ejecución</Typography>
                                </React.Fragment>
                            }
                        >
                            <IconButton
                                className="ml-3"
                                style={{
                                    cursor: !IsThereSpecifications() ? "not-allowed" : "hand"
                                }}
                                onClick={e => {
                                    GetSpecDocuments()
                                }}>
                                <Article />
                            </IconButton>
                        </HtmlTooltip> : []
                }

                {!IsThereSpecifications() ? <h5 className="mt-5 ml-5">No existen especificaciones registradas.</h5> : []}

                {
                    !showSpecForm ?
                        TreeExecutions
                        :
                        <Select
                            value={executionValue}
                            onChange={e => {
                                SelectFormComponent(e.target.value);
                                setExecutionValue(e.target.value);
                            }}
                            input={<OutlinedInput sx={{ width: "350px" }} label="Ejecución" />}
                        >
                            {(specifications && !specifications.TLS) || (specifications.TLS && specifications.TLS.length <= 0) ? <MenuItem value={0} >TLS</MenuItem> : []}
                            {(specifications && !specifications.JWT) || (specifications.JWT && specifications.JWT.length <= 0) ? <MenuItem value={1} >JWT</MenuItem> : []}
                            {(specifications && !specifications.CA) || (specifications.CA && !specifications.CA.ambiente) || (specifications.CA && specifications.CA.ambiente && specifications.CA.ambiente.length <= 0) ? <MenuItem value={2} >Configurador de Ambiente [ambiente]</MenuItem> : []}
                            {(specifications && !specifications.SharedFlow) || (specifications.SharedFlow && specifications.SharedFlow.length <= 0) ? <MenuItem value={3} >SharedFlow</MenuItem> : []}
                            {(specifications && !specifications.API) || (specifications.API && specifications.API.length <= 0) ? <MenuItem value={4} >API(s)</MenuItem> : []}
                            {(specifications && !specifications.CA) || (specifications.CA && !specifications.CA.componentes) || (specifications.CA && specifications.CA.componentes && specifications.CA.componentes.length <= 0) ? <MenuItem value={5} >Configurador de Ambiente [componentes]</MenuItem> : []}
                            {(specifications && !specifications.CA) || (specifications.CA && !specifications.CA.app) || (specifications.CA && specifications.CA.app && specifications.CA.app.length <= 0) ? <MenuItem value={6} >Configurador de Ambiente [app]</MenuItem> : []}
                            {(specifications && !specifications.CA) || (specifications.CA && !specifications.CA["app-key"]) || (specifications.CA && specifications.CA["app-key"] && specifications.CA["app-key"].length <= 0) ? <MenuItem value={7} >Configurador de Ambiente [app-key]</MenuItem> : []}
                            {(specifications && !specifications.CA) || (specifications.CA && !specifications.CA.producto) || (specifications.CA && specifications.CA.producto && specifications.CA.producto.length <= 0) ? <MenuItem value={8} >Configurador de Ambiente [producto]</MenuItem> : []}
                            {specifications && !specifications.CURLS ? <MenuItem value={9} >CURLs de validación</MenuItem> : []}
                            {specifications && !specifications.reverseStrategy ? <MenuItem value={10} >Plan de reverso</MenuItem> : []}
                        </Select>
                }
            </dt>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={EditingForm.showSpecEditing}
                onClose={handleCloseModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={EditingForm.showSpecEditing}>
                    <Box sx={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: '60%',
                        bgcolor: 'background.paper',
                        border: '2px solid #000',
                        boxShadow: 24,
                        p: 4,
                    }}>
                        <div className="card">
                            <div className="card-header row">
                                <FormLabel className="col-11" id="form_title">{EditingForm.title}</FormLabel>
                                <IconButton className="mb-3 float-right col-1" onClick={handleCloseModal}>
                                    <Close />
                                </IconButton>
                            </div>
                            <div className="card-body">
                                {EditingForm.component}
                            </div>
                        </div>
                    </Box>
                </Fade>
            </Modal>
        </dl >
    );
}
export default ExecutionSpec;