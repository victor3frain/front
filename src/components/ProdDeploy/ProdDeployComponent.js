import React, { useState, useEffect, useRef } from "react";
import Settings from '../Utils/configsettings.js';
import Utils from '../Utils/Utils.js';
import ExecutionSpec from "./ExecutionSpec.js";

//Axios
import axios from 'axios';

//Microsoft SignalR
import { HubConnectionBuilder } from '@microsoft/signalr';

//Sweet alert 2
import Swal from 'sweetalert2';

//Full Calendar
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import esLocale from '@fullcalendar/core/locales/es';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';

//MUI
import {
    Button, Modal, Box, Fade, Backdrop, Divider, Chip, IconButton, Autocomplete, TextField, Select,
    MenuItem, OutlinedInput
} from '@mui/material';
import {
    Groups, Check, Construction, AssignmentTurnedIn, Error, Edit, FactCheck, EventAvailable, Pending,
    Delete, Save, Cancel, Info, CheckCircle, CloudOff, Close, PendingActions, LibraryAddCheck, BackupTable
} from '@mui/icons-material';
import LoadingButton from '@mui/lab/LoadingButton';
import OrderExecution from "./Executions/OrderExecution.js";

function ProdDeployComponent(props) {
    const LINKJAZZ = "https://ccm.colaboracionsimba.net:9443/ccm/web/projects/Gobierno%20de%20APIs#action=com.ibm.team.workitem.viewWorkItem&id=OT_NUMBER%20usuario%20de%20jazz"
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    //const RegexTitle = "^(\\d+\\.\\ )";

    const calendar = useRef();
    const deployTitle = useRef();
    const deployJira = useRef();
    const deployDescription = useRef();
    const deployOTNumber = useRef();
    const [deployDetails, setDeployDetails] = useState({
        showDetails: false,
        details: {}
    });

    const [editing, setEditing] = useState(false);
    const [connection, setConnection] = useState(null);
    const [refresh, setRefresh] = useState(false);
    const [searchingDate, setSearchingDate] = useState({
        initialDate: null,
        finalDate: null
    })
    const [ShowOrderExecution, setShowOrderExecution] = useState(false);

    //Update a deploy variable
    const deployUpdatedOt = useRef();
    const deployUpdatedDescription = useRef();
    const [selectedUsers, setSelectedUsers] = useState({
        devops: null,
        developer1: null,
        developer2: null,
        team: null
    })
    const deployTitleOT = useRef();
    const deployTitleOTForm = useRef();
    const deployUpdatedOtTitle = useRef();
    const deployUpdatedJira = useRef();

    const [editActive, setEditActive] = useState(false);

    const [events, setEvents] = useState({
        externalEvents: [],
        acceptedDeploys: []
    });
    const [users, setUsers] = useState({
        devops: [],
        developer1: "",
        developer2: ""
    })
    const [inputValue, setInputValue] = useState("");

    const permissions = {
        edit: props.user.role && props.user.role.toUpperCase() === "DEVOPS",
        create: props.user.role && (props.user.role.toUpperCase() === "DEVOPS" || props.user.role.toUpperCase() === "ARCHITECT"),
        delete: props.user.role && (props.user.role.toUpperCase() === "DEVOPS")
    }

    const calendarProps = {
        plugins: [dayGridPlugin, timeGridPlugin, listPlugin, bootstrapPlugin, interactionPlugin],
        initialView: 'dayGridMonth',
        locale: esLocale,
        height: props.user.role && props.user.role.toUpperCase() === "DEVELOPER" ? "700px" : "100%",
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,listWeek'
        },
        themeSystem: "bootstrap",
        bootstrapFontAwesome: {
            close: 'fa-times',
            prev: 'fa-chevron-left',
            next: 'fa-chevron-right',
            prevYear: 'fa-angle-double-left',
            nextYear: 'fa-angle-double-right'
        },
        droppable: permissions.edit,
        editable: permissions.edit,
        eventSources: [
            {
                events: events.acceptedDeploys
            }
        ],
        eventResize: function (info) {
            const deploy = {
                id: info.event.id,
                end: info.event.end,
                date: info.event.start
            }
            updateDeploy(deploy)
        },
        timeZone: "local",
        eventClick: function (info) {
            GetDeploy(info.event.id)
        },
        eventDrop: function (info) {
            let idEvent = info.event.id,
                updatedEvents = {
                    acceptedDeploys: events.acceptedDeploys,
                    externalEvents: events.externalEvents
                }
            const acceptedDeployIndex = updatedEvents.acceptedDeploys.findIndex(accDep => accDep.id === idEvent);
            const acceptedDeploy = updatedEvents.acceptedDeploys.find(penDep => penDep.id === idEvent);
            const deploy = {
                id: idEvent,
                title: acceptedDeploy.title,
                color: acceptedDeploy.color,
                textColor: acceptedDeploy.textColor,
                link: acceptedDeploy.link,
                start: info.event.start.getFullYear() + '-' + (info.event.start.getMonth() + 1 < 10 ? '0' + (info.event.start.getMonth() + 1) : (info.event.start.getMonth() + 1)) + '-' + (info.event.start.getDate() < 10 ? '0' + info.event.start.getDate() : info.event.start.getDate()),
                end: info.event.end ? info.event.end.getFullYear() + '-' + (info.event.end.getMonth() + 1 < 10 ? '0' + (info.event.end.getMonth() + 1) : (info.event.end.getMonth() + 1)) + '-' + (info.event.end.getDate() < 10 ? '0' + info.event.end.getDate() : info.event.end.getDate()) : null,
                extendedProps: {
                    status: 1
                }
            }
            updatedEvents.acceptedDeploys[acceptedDeployIndex] = deploy;
            let cal = calendar.current.getApi();
            cal.removeAllEvents();
            updateDeploy(deploy);
            setEvents(updatedEvents);
        },
        eventReceive: (info) => {
            let updatedEvents = {
                acceptedDeploys: events.acceptedDeploys,
                externalEvents: events.externalEvents
            },
                idEvent = info.event.id;
            const pendingDeploy = updatedEvents.externalEvents.find(penDep => penDep.id === idEvent);
            if (pendingDeploy) {
                const deploy = {
                    id: idEvent,
                    title: pendingDeploy.title,
                    color: pendingDeploy.color,
                    textColor: pendingDeploy.textColor,
                    link: pendingDeploy.link,
                    start: info.event.start.getFullYear() + '-' + (info.event.start.getMonth() + 1 < 10 ? '0' + (info.event.start.getMonth() + 1) : (info.event.start.getMonth() + 1)) + '-' + (info.event.start.getDate() < 10 ? '0' + info.event.start.getDate() : info.event.start.getDate()),
                    extendedProps: {
                        status: 1
                    }
                }
                updatedEvents.acceptedDeploys.push(deploy)
                const pendingUpdatedDeploy = updatedEvents.externalEvents.filter((deploy) => deploy.id !== idEvent);
                updatedEvents.externalEvents = pendingUpdatedDeploy;
                calendar.current.getApi().removeAllEvents();
                updateDeploy(deploy);
                setEvents(updatedEvents);
            }
            else {
                info.event.remove();
                return;
            }
        }
    }

    const handleCloseShowingOrderExecution = () => {
        setShowOrderExecution(false);
    };

    function GetDeploy(id) {
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/getone/" + id,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                const deploy = data.body
                setDeployDetails({
                    details: deploy,
                    showDetails: true
                });

                var teams = Settings.GetTeams();
                setSelectedUsers({
                    team: { id: deploy.team, name: teams.find(team => team.id === deploy.team).name },
                    developer1: !deploy.developer1 ? deploy.developer : deploy.developer1 ? deploy.developer1 : "",
                    developer2: deploy.developer2 ? deploy.developer2 : "",
                    devops: deploy.devops
                })
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    function showDeploydetails(showingDeployDetails, details) {
        setDeployDetails({
            showDetails: showingDeployDetails,
            details: details
        });
    }

    function updateEvent(event) {
        var pendingDeploys = events.externalEvents,
            acceptedDeploys = events.acceptedDeploys;

        var eventIndex = pendingDeploys.findIndex(pendingDeploy => pendingDeploy.id === event.id);

        const colorTeam = GetColorTeam(event.team);
        if (eventIndex >= 0) {
            pendingDeploys[eventIndex].custom = event.description;
            pendingDeploys[eventIndex].link = event.link;
            pendingDeploys[eventIndex].color = colorTeam.color;
            pendingDeploys[eventIndex].textColor = colorTeam.textColor;
        }

        eventIndex = acceptedDeploys.findIndex(acceptedDeploy => acceptedDeploy.id === event.id);

        if (eventIndex >= 0) {
            acceptedDeploys[eventIndex].link = event.link;
            acceptedDeploys[eventIndex].color = colorTeam.color;
            acceptedDeploys[eventIndex].textColor = colorTeam.textColor;
        }

        setEvents({
            acceptedDeploys: acceptedDeploys,
            externalEvents: pendingDeploys
        })
    }

    function updateDeploy(deploy, justEditing) {
        /*if(!deploy.title.match(RegexTitle)){
            Toast.fire({
                icon: 'error',
                title: 'Título de unidad de negocio no cuenta con un formato correcto',
                target: document.getElementById("ProdDeploy")
            });
            setEditing(false);
            return;
        }*/
        axios({
            method: 'put',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/update",
            data: deploy,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error,
                    target: document.getElementById("OrderExecution")
                });
            }
            else {
                Toast.fire({
                    icon: 'success',
                    title: 'Despliegue ' + (justEditing ? 'editado' : 'agendado'),
                    target: document.getElementById("OrderExecution")
                });
                setEditActive(false);
                setEditing(false);
                if (justEditing) {
                    setDeployDetails({
                        showDetails: deployDetails.showDetails,
                        details: {
                            _id: deploy.id,
                            buildingStatus: deploy.buildingStatus,
                            date: deploy.date,
                            description: deploy.description,
                            developer1: deploy.developer1,
                            developer2: deploy.developer2,
                            devops: deploy.devops,
                            link: deploy.link,
                            notes: deploy.notes,
                            otNumber: deploy.otNumber,
                            status: deploy.status,
                            team: deploy.team,
                            title: deploy.title,
                            titleOt: deploy.titleOt,
                            jira: deploy.jira
                        }
                    })

                    updateEvent(deploy)
                    GetDeploys(searchingDate.initialDate, searchingDate.finalDate);
                }
                SendNotification(props.user.name + " " + props.user.lastName, "actualizó un despliegue")
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al actualizar el despliegue (' + error.message + ')',
                target: document.getElementById("OrderExecution")
            })
            setEditing(false);
        });
    }

    function GetColorTeam(team) {
        const teamDefinition = Settings.GetTeams().find(teamId => teamId.id === team);
        var ColorTeam = {
            color: teamDefinition.color,
            textColor: teamDefinition.textColor
        };

        return ColorTeam;
    }

    function CreateAcceptedDeploys(DataDeploy) {
        var date;
        var ColorTeam = GetColorTeam(DataDeploy.team);
        date = new Date(DataDeploy.date);
        var finalDate = DataDeploy.finalDate ? new Date(DataDeploy.finalDate) : date;
        const deploy = {
            id: DataDeploy._id,
            title: DataDeploy.title,
            color: ColorTeam.color,
            textColor: ColorTeam.textColor,
            start: date.getFullYear() + '-' + (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
            end: finalDate.getFullYear() + '-' + (finalDate.getMonth() + 1 < 10 ? '0' + (finalDate.getMonth() + 1) : (finalDate.getMonth() + 1)) + '-' + (finalDate.getDate() < 10 ? '0' + finalDate.getDate() : finalDate.getDate()),
            extendedProps: {
                status: DataDeploy.status
            }
        }
        return deploy;
    }

    function CreatePendingDeploys(DataDeploy) {
        const ColorTeam = GetColorTeam(DataDeploy.team);
        return {
            id: DataDeploy._id,
            title: DataDeploy.title,
            color: ColorTeam.color,
            textColor: ColorTeam.textColor,
            custom: DataDeploy.description,
            link: DataDeploy.link
        }
    }

    const GetDeploys = (InitialDate, FinalDate) => {
        const inidate = InitialDate.toISOString();
        const finDate = FinalDate.toISOString();
        axios({
            method: 'get',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/period/" + inidate + "/" + finDate,
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                var gettingEvents = {
                    externalEvents: [],
                    acceptedDeploys: []
                }
                for (var acceptedDeploy of data.body.acceptedDeploys) {
                    gettingEvents.acceptedDeploys.push(CreateAcceptedDeploys(acceptedDeploy));
                }

                for (var pendingDeploy of data.body.pendingDeploys) {
                    gettingEvents.externalEvents.push(CreatePendingDeploys(pendingDeploy));
                }
                setEvents(gettingEvents);
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al obtener los despliegues (' + error.message + ')'
            })
        });
    }

    function SubmitPendingDeploy(e) {
        e.preventDefault();
        /*
        if (!deployTitle.current.value.match(RegexTitle)) {
            Toast.fire({
                icon: 'error',
                title: 'El título de unidad de negocio no cuenta con el formato correcto'
            });
            return;
        }*/
        var ColorTeam = GetColorTeam(props.user.team);
        var pendingDeploy = {
            title: deployTitle.current.value,
            color: ColorTeam.color,
            textColor: ColorTeam.textColor,
            custom: deployDescription.current.value,
            link: !deployOTNumber.current.value ? null : LINKJAZZ.replace("OT_NUMBER", deployOTNumber.current.value)
        };
        axios({
            method: 'post',
            url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/create",
            data: {
                ...pendingDeploy,
                titleOt: deployTitleOTForm.current.value ? deployTitleOTForm.current.value : null,
                otNumber: deployOTNumber.current.value <= 0 ? null : deployOTNumber.current.value,
                team: props.user.team,
                description: deployDescription.current.value,
                jira: deployJira.current.value,
                status: 0
            },
            responseType: 'json'
        }).then(response => {
            var data = response.data;
            if (data.error) {
                Toast.fire({
                    icon: 'error',
                    title: 'Error: ' + data.error
                });
            }
            else {
                let newEvents = {
                    externalEvents: events.externalEvents,
                    acceptedDeploys: events.acceptedDeploys
                }
                pendingDeploy.id = data.body;
                newEvents.externalEvents.push(pendingDeploy);
                setEvents(newEvents);
                Toast.fire({
                    icon: 'success',
                    title: 'Despliegue generado'
                });
                SendNotification(props.user.name + " " + props.user.lastName, "generó un despliegue nuevo")
            }
        }).catch(error => {
            console.log(error.message)
            Toast.fire({
                icon: 'error',
                title: 'Ocurrión un error al actualizar el despliegue (' + error.message + ')'
            })
        });
    }

    const handleCloseModal = () => {
        if (!editing) {
            showDeploydetails(false, deployDetails.details)
            setEditActive(false);
            setEditing(false);
            ResetValues();
        }
    };

    const GetTeamIcon = () => {
        const team = Settings.GetTeams().find(teamReference => teamReference.id === deployDetails.details.team);
        return <Chip style={{
            backgroundColor: team.color,
            color: team.textColor
        }} icon={<Groups style={{ color: team.textColor }} />} label={team.name} />
    }

    function DeleteDeploy(id) {
        setDeployDetails({
            details: deployDetails.details,
            showDetails: false
        })
        Swal.fire({
            title: '¿Estás seguro que quieres eliminar el despliegue?',
            text: "Se eliminará por completo la información del despliegue",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                axios({
                    method: 'delete',
                    url: Settings.GetApiIp() + "/CoeTicketDeploy/deploy/delete/" + id,
                    responseType: 'json'
                }).then(response => {
                    var data = response.data;
                    if (data.error) {
                        Toast.fire({
                            icon: 'error',
                            title: 'Error: ' + data.error
                        });
                    }
                    else {
                        const deployDetails = {
                            details: {},
                            showDetails: false
                        }
                        const newAcceptedDeploysList = events.acceptedDeploys.filter(acceptedDeploy => acceptedDeploy.id !== id);
                        const newExternalDeploysList = events.externalEvents.filter(pendingDeploy => pendingDeploy.id !== id);
                        const newEvents = {
                            externalEvents: newExternalDeploysList,
                            acceptedDeploys: newAcceptedDeploysList
                        }
                        setDeployDetails(deployDetails);
                        setEvents(newEvents);
                        Toast.fire({
                            icon: 'success',
                            title: 'Despliegue eliminado'
                        });
                        SendNotification(props.user.name + " " + props.user.lastName, "eliminó un despliegue")
                    }
                }).catch(error => {
                    console.log(error.message)
                    Toast.fire({
                        icon: 'error',
                        title: 'Ocurrión un error al actualizar el despliegue (' + error.message + ')'
                    })
                });
            }
        })
    }

    /*
    const GetDevelopers = (event, newInputValue) => {
        if (inputValue.length === 1 && !newInputValue) {
            setInputValue("")
            setUsers({
                developer1: [],
                developer2: [],
                devops: users.devops
            })
        }
        else if (newInputValue) {
            setInputValue(newInputValue)
            axios({
                method: 'get',
                url: Settings.GetApiIp() + "/CoeTicketSec/user/developers/" + Utils.encodeBase64(newInputValue),
                responseType: 'json'
            }).then(response => {
                var data = response.data;
                if (data.error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error: ' + data.error
                    });
                }
                else {
                    setUsers({
                        developer: data.body,
                        devops: users.devops
                    })
                    setInputValue(newInputValue);
                }
            }).catch(error => {
                console.log(error.message)
                Toast.fire({
                    icon: 'error',
                    title: 'Ocurrión un error al obtener el equipo devops (' + error.message + ')'
                })
            });
        }
    }*/

    function checkEdit() {
        if (editActive) {
            setEditing(true)
            updateDeploy(CreateDeployObject("UPDATINGDEPLOY"), true);
        }
        else {
            axios({
                method: 'get',
                url: Settings.GetApiIp() + "/CoeTicketSec/user/devops/",
                responseType: 'json'
            }).then(response => {
                var data = response.data;
                if (data.error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Error: ' + data.error
                    });
                }
                else {
                    setUsers({
                        developer1: users.developer1,
                        developer2: users.developer2,
                        devops: data.body
                    })
                    setEditActive(true);
                    ResetValues();
                }
            }).catch(error => {
                console.log(error.message)
                Toast.fire({
                    icon: 'error',
                    title: 'Ocurrión un error al obtener el equipo devops (' + error.message + ')'
                })
            });
        }
    }

    function ResetValues() {
        setSelectedUsers({
            devops: deployDetails.details.devops,
            developer1: deployDetails.details.developer1,
            developer2: deployDetails.details.developer2,
            team: { id: deployDetails.details.team, name: Settings.GetTeams().find(team => team.id === deployDetails.details.team).name }
        });
    }

    function CreateDeployObject(option, currentStatus) {
        var customDate = new Date(deployDetails.details.date);
        customDate.setHours(1);
        customDate = customDate.toISOString();
        var deploy = {
            id: deployDetails.details._id,
            title: deployDetails.details.title,
            titleOt: deployDetails.details.titleOt,
            otNumber: deployDetails.details.otNumber,
            jira: deployDetails.details.jira,
            date: customDate,
            link: deployDetails.details.link,
            devops: deployDetails.details.devops,
            developer1: deployDetails.details.developer1,
            developer2: deployDetails.details.developer2,
            description: deployDetails.details.description,
            team: deployDetails.details.team,
            notes: deployDetails.details.notes
        };
        switch (option.toUpperCase()) {
            case 'DEPLOYDETAILS':
                deploy = {
                    ...deploy,
                    buildingStatus: currentStatus,
                    status: deployDetails.details.status === 1 ? 2 : currentStatus === 0 ? 1 : 2
                }
                break;
            case 'UPDATINGDEPLOY':
                deploy = {
                    id: deployDetails.details._id,
                    title: !deployTitleOT.current.value ? deployDetails.details.title : deployTitleOT.current.value,
                    titleOt: !deployUpdatedOtTitle.current.value ? null : deployUpdatedOtTitle.current.value,
                    jira: !deployUpdatedJira.current.value ? null : deployUpdatedJira.current.value,
                    otNumber: deployUpdatedOt.current.value === "" ? null : deployUpdatedOt.current.value,
                    date: customDate,
                    link: deployUpdatedOt.current.value === "" ? null : LINKJAZZ.replace("OT_NUMBER", deployUpdatedOt.current.value),
                    devops: !selectedUsers.devops ? null : { id: selectedUsers.devops.id, name: selectedUsers.devops.name, lastName: selectedUsers.devops.lastName },
                    developer1: deployDetails.details.developer1,
                    developer2: deployDetails.details.developer2,
                    description: deployUpdatedDescription.current.value === "" ? null : deployUpdatedDescription.current.value,
                    team: selectedUsers.team.id,
                    buildingStatus: deployDetails.details.buildingStatus,
                    notes: deployDetails.details.notes,
                    status: deployDetails.details.status
                }
                break;
            case 'DEPLOYDETAILSSTATUS':
                deploy = {
                    ...deploy,
                    buildingStatus: deployDetails.details.buildingStatus,
                    status: currentStatus
                }
                break;
            default: break;
        }
        return deploy;
    }

    const SendNotification = async (user, message) => {
        const notification = {
            User: user,
            NotificationBody: message,
            UserId: Utils.getCookie('user')
        };
        if (connection._connectionStarted) {
            try {
                await connection.send('SendNotification', notification);
            }
            catch (e) {
                console.log(e);
            }
        }
        else {
            Toast.fire({
                icon: 'error',
                title: 'No connection to server yet.'
            });
        }
    }

    function GetInitialFinalDate(changingDate, quantity, date) {
        var InitialDate, FinalDate;
        switch (changingDate.toUpperCase()) {
            case "DAYGRIDMONTH":
                InitialDate = new Date(date.getFullYear(), date.getMonth() + quantity - 1, 1);
                FinalDate = new Date(date.getFullYear(), date.getMonth() + quantity + 2, 0);
                break;
            case "LISTWEEK":
            case "TIMEGRIDWEEK":
                InitialDate = new Date(date.toISOString());
                var day = date.getDay(),
                    diff = date.getDate() - day + (day === 0 ? -6 : 1);
                InitialDate = new Date(InitialDate.setDate(diff));
                InitialDate.setDate(InitialDate.getDate() + (quantity * 7));
                FinalDate = new Date(InitialDate.toISOString());
                FinalDate.setDate(FinalDate.getDate() + 6);
                break;
            case "DAY":
                InitialDate = new Date(date.getDate() + quantity);
                FinalDate = new Date(InitialDate.toISOString());
                FinalDate.setDate(FinalDate.getDate() + 1);
                break;
            default: break;
        }
        return { InitialDate, FinalDate }
    }

    const GetStatusDeployComponent = (statusDeploy) => {
        var component;
        switch (statusDeploy) {
            case 0:
                component = GetPreStatus();
                break;
            case 1:
                component = <Chip className="ml-3" style={{
                    backgroundColor: "green",
                    color: "white"
                }}
                    label="Liberado"
                    icon={<Check style={{ color: "white" }} />} />
                break;
            case 2:
                component = <Chip className="ml-3" style={{
                    backgroundColor: "chocolate",
                    color: "white"
                }}
                    label="En construcción"
                    icon={<Construction style={{ color: "white" }} />} />
                break;
            case 3:
                component = <Chip className="ml-3" style={{
                    backgroundColor: "steelblue",
                    color: "white"
                }}
                    label="Depliegue terminado"
                    icon={<AssignmentTurnedIn style={{ color: "white" }} />} />
                break;
            case 4:
                component = <Chip className="ml-3" style={{
                    backgroundColor: "grey",
                    color: "white"
                }}
                    label="Depliegue terminado"
                    icon={<CloudOff style={{ color: "white" }} />} />
                break;
            default:
                component = <Chip className="ml-3" style={{
                    backgroundColor: "red",
                    color: "white"
                }}
                    label="Error al obtener el status"
                    icon={<Error style={{ color: "white" }} />} />
                break;
        }
        return component;
    }

    const GetPreStatus = () => {
        var component = [];
        switch (deployDetails.details.status) {
            case 0:
                component = (
                    <Chip className="ml-3" style={{
                        backgroundColor: "#5FAAE3",
                        color: "white"
                    }}
                        label="Pendiente por confirmar"
                        icon={<PendingActions style={{ color: "white" }} />} />
                );
                break;
            case 1:
                component = (
                    <Chip className="ml-3" style={{
                        backgroundColor: "#82befd",
                        color: "black"
                    }}
                        label="OT Agendada"
                        icon={<EventAvailable style={{ color: "black" }} />} />
                );
                break;
            case 3:
                component = (
                    <Chip className="ml-3" style={{
                        backgroundColor: "#36AB9D",
                        color: "white"
                    }}
                        label="Confirmada"
                        icon={<LibraryAddCheck style={{ color: "white" }} />} />
                );
                break;

            case 4:
                component = (
                    <Chip className="ml-3" style={{
                        backgroundColor: "#007a71",
                        color: "white"
                    }}
                        label="Plantilla lista"
                        icon={<FactCheck style={{ color: "white" }} />} />
                );
                break;
            case 5:
                component = (
                    <Chip className="ml-3" style={{
                        backgroundColor: "#004015",
                        color: "white"
                    }}
                        label="OT Generada"
                        icon={<AssignmentTurnedIn style={{ color: "white" }} />} />
                );
                break;
            default: break;
        }
        return component;
    }

    function GetSelectionStatus() {
        var component = [];
        if (!editActive && props.user.role && props.user.role.toUpperCase() !== "DEVELOPER" &&
            (deployDetails.details.status === 1 || deployDetails.details.status === 2)) {
            component = (
                <Select
                    className="mr-1 flex-left"
                    value={deployDetails.details.buildingStatus}
                    onChange={e => updateDeploy(CreateDeployObject("DEPLOYDETAILS", e.target.value), true)}
                    input={<OutlinedInput label="Status" />}
                    MenuProps={{
                        PaperProps: {
                            style: {
                                width: 250,
                            },
                        },
                    }}
                >
                    <MenuItem value={0} >
                        <Chip className="ml-3" style={{
                            backgroundColor: "#17538c",
                            color: "white"
                        }}
                            label="Por construir"
                            icon={<Pending style={{ color: "white" }} />} />
                    </MenuItem>
                    <MenuItem value={2} >
                        <Chip className="ml-3" style={{
                            backgroundColor: "chocolate",
                            color: "white"
                        }}
                            label="En construcción"
                            icon={<Construction style={{ color: "white" }} />} />
                    </MenuItem>
                    <MenuItem value={1} >
                        <Chip className="ml-3" style={{
                            backgroundColor: "green",
                            color: "white"
                        }}
                            label="liberado"
                            icon={<Check style={{ color: "white" }} />} />
                    </MenuItem>
                    {props.user.role && props.user.role.toUpperCase() === "DEVOPS" ? <MenuItem value={3} >
                        <Chip className="ml-3" style={{
                            backgroundColor: "steelblue",
                            color: "white"
                        }}
                            label="Depliegue terminado"
                            icon={<AssignmentTurnedIn style={{ color: "white" }} />} />
                    </MenuItem> : []}
                    {props.user.role && props.user.role.toUpperCase() === "DEVOPS" ?
                        <MenuItem value={4} >
                            <Chip className="ml-3" style={{
                                backgroundColor: "grey",
                                color: "white"
                            }}
                                label="Reversado"
                                icon={<CloudOff style={{ color: "white" }} />} />
                        </MenuItem> : []
                    }
                </Select>
            )
        }
        else
            if (!editActive && props.user.role && props.user.role.toUpperCase() === "DEVOPS" &&
                (deployDetails.details.status !== 1 && deployDetails.details.status !== 2)) {
                component = (
                    <Select
                        className="mr-1 flex-left"
                        value={deployDetails.details.status}
                        onChange={e => updateDeploy(CreateDeployObject("DEPLOYDETAILSSTATUS", e.target.value), true)}
                        input={<OutlinedInput label="Status" />}
                        MenuProps={{
                            PaperProps: {
                                style: {
                                    width: 250,
                                },
                            },
                        }}
                    >
                        <MenuItem value={0} >
                            <Chip className="ml-3" style={{
                                backgroundColor: "#5FAAE3",
                                color: "white"
                            }}
                                label="Pendiente por confirmar"
                                icon={<PendingActions style={{ color: "white" }} />} />
                        </MenuItem>
                        <MenuItem value={3} >
                            <Chip className="ml-3" style={{
                                backgroundColor: "#36AB9D",
                                color: "white"
                            }}
                                label="Confirmada"
                                icon={<LibraryAddCheck style={{ color: "white" }} />} />
                        </MenuItem>
                        <MenuItem value={4} >
                            <Chip className="ml-3" style={{
                                backgroundColor: "#007a71",
                                color: "white"
                            }}
                                label="Plantilla lista"
                                icon={<FactCheck style={{ color: "white" }} />} />
                        </MenuItem>
                        <MenuItem value={5} >
                            <Chip className="ml-3" style={{
                                backgroundColor: "#004015",
                                color: "white"
                            }}
                                label="OT Generada"
                                icon={<AssignmentTurnedIn style={{ color: "white" }} />} />
                        </MenuItem>
                    </Select>
                )
            }
        return component;
    }

    useEffect(() => {
        var draggableEl = document.getElementById("external-events");
        if (permissions.edit) {
            new Draggable(draggableEl, {
                itemSelector: ".fc-event",
                eventData: function (eventEl) {
                    let id = eventEl.dataset.id;
                    let title = eventEl.getAttribute("title");
                    let color = eventEl.dataset.color;
                    let custom = eventEl.dataset.custom;

                    return {
                        id: id,
                        title: title,
                        color: color,
                        custom: custom,
                        create: true
                    };
                }
            });
        }
    }, [events])

    useEffect(() => {
        const today = new Date();
        var month = today.getMonth(),
            year = today.getFullYear();
        setSearchingDate({ initialDate: new Date(year, month - 1, 1), finalDate: new Date(year, month + 2, 0) })
        GetDeploys(new Date(year, month - 1, 1), new Date(year, month + 2, 0));

        const newConnection = new HubConnectionBuilder()
            .withUrl(Settings.GetApiIp() + '/hubs/notification')
            .withAutomaticReconnect()
            .build();

        setConnection(newConnection);

        var prevButton = document.getElementsByClassName('fc-prev-button');
        if (prevButton && calendar) {
            prevButton[0].onclick = function () {
                var calendarMethods = calendar.current.getApi();
                var calendarDate = calendarMethods.getDate();
                const dates = GetInitialFinalDate(calendarMethods.view.type, -1, calendarDate);
                setSearchingDate({ initialDate: dates.InitialDate, finalDate: dates.FinalDate })
                GetDeploys(dates.InitialDate, dates.FinalDate);
            };
        }

        var nextButton = document.getElementsByClassName('fc-next-button');
        if (nextButton && calendar) {
            nextButton[0].onclick = function () {
                var calendarMethods = calendar.current.getApi();
                var calendarDate = calendarMethods.getDate();
                const dates = GetInitialFinalDate(calendarMethods.view.type, 1, calendarDate);
                setSearchingDate({ initialDate: dates.InitialDate, finalDate: dates.FinalDate })
                GetDeploys(dates.InitialDate, dates.FinalDate);
            };
        }

        var todayButton = document.getElementsByClassName('fc-today-button');
        if (todayButton && calendar) {
            todayButton[0].onclick = function () {
                var calendarMethods = calendar.current.getApi();
                var date = new Date();
                const dates = GetInitialFinalDate(calendarMethods.view.type, 0, date);
                setSearchingDate({ initialDate: dates.InitialDate, finalDate: dates.FinalDate })
                GetDeploys(dates.InitialDate, dates.FinalDate);
            }
        }

    }, []);

    useEffect(() => {
        if (connection) {
            connection.start()
                .then(result => {
                    console.log('Connected!');

                    connection.on('RecieveNotification', (notification) => {
                        const user = Utils.getCookie('user');
                        if (notification.userId !== user && user) {
                            Toast.fire({
                                icon: 'warning',
                                title: notification.user + " " + notification.notificationBody
                            })
                            setRefresh(true)
                        }
                    });
                })
                .catch(e => console.log('Connection failed: ', e));
        }
    }, [connection]);

    return (
        <div className="row">
            {permissions.create || permissions.edit ?
                <div className="col-3">
                    <div className="sticky-top mb-3">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-titile">
                                    Despliegues {refresh ?
                                        <Button
                                            className="ml-5"
                                            onClick={e => {
                                                setRefresh(false)
                                                GetDeploys(searchingDate.initialDate, searchingDate.finalDate)
                                            }}>Actualizar</Button>
                                        : []}
                                </h4>
                            </div>
                            <div id="external-events" className="card-body">
                                {events.externalEvents.length > 0 ?
                                    events.externalEvents.map((event) => (
                                        <div
                                            className="fc-event fc-h-event mb-1 fc-daygrid-event fc-daygrid-block-event p-2"
                                            title={event.title}
                                            data-id={event.id}
                                            data-color={event.color}
                                            data-custom={event.custom}
                                            key={event.id}
                                            style={{
                                                backgroundColor: event.color,
                                                borderColor: event.color,
                                                cursor: "pointer"
                                            }}
                                        >
                                            <div className="fc-event-main">
                                                <div>
                                                    <strong style={{ color: event.textColor }}>{event.title}</strong>
                                                    <IconButton
                                                        size="small"
                                                        onClick={e => GetDeploy(event.id)}>
                                                        <Info />
                                                    </IconButton>
                                                </div>
                                                <p style={{ color: event.textColor }}>{event.custom}</p>
                                                {event.link ?
                                                    <a
                                                        className="hover_class"
                                                        href={event.link} style={
                                                            {
                                                                color: event.textColor
                                                            }
                                                        } target="_BLANK" rel="noopener noreferrer">Detalles
                                                    </a>
                                                    : []
                                                }
                                            </div>
                                        </div>
                                    )) :
                                    <p>No existen despliegues por agendar</p>
                                }
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-titile">
                                    Solicitar despliegue
                                </h4>
                            </div>
                            {permissions.create ?
                                <div className="card-body">
                                    <form onSubmit={e => SubmitPendingDeploy(e)}>
                                        <div className="form-group">
                                            <label>Título de unidad de negocio</label>
                                            <input type="text"
                                                ref={deployTitle}
                                                className="form-control form-control-border"
                                                placeholder="Título de unidad de negocio"
                                                required />
                                        </div>
                                        <div className="form-group">
                                            <label>Título de la OT </label>
                                            <input
                                                ref={deployTitleOTForm}
                                                type="text"
                                                className="form-control form-control-border"
                                                placeholder="Título de la OT" />
                                        </div>
                                        <div className="row">
                                            <div className="col-6 form-group">
                                                <label>Número de OT</label>
                                                <input
                                                    ref={deployOTNumber}
                                                    type="number"
                                                    className="form-control form-control-border"
                                                    placeholder="Número de OT" />
                                            </div>
                                            <div className="col-6 form-group">
                                                <label>Tarea de Jira</label>
                                                <input
                                                    ref={deployJira}
                                                    type="text"
                                                    className="form-control form-control-border"
                                                    placeholder="Tarea de Jira" />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Descripción del cambio explícito</label>
                                            <textarea
                                                ref={deployDescription}
                                                className="form-control"
                                                rows="3"
                                                placeholder="Descripción"></textarea>
                                        </div>
                                        <div className="form-group row">
                                            <button type="submit" className="btn btn-primary col-sm-12 col-md-4 col-lg-4 col-xl-4 mr-3">SOLICITAR</button>
                                            {
                                                props.user.role.toUpperCase() === "DEVOPS" ?
                                                    <Button
                                                        className="mt-1"
                                                        color="success"
                                                        variant="contained"
                                                        startIcon={<BackupTable />}
                                                        onClick={e => setShowOrderExecution(true)}>
                                                        Orden de ejecución
                                                    </Button>
                                                    : []
                                            }
                                        </div>
                                    </form>
                                </div>
                                : []
                            }
                        </div>
                    </div>
                </div>
                : <div />
            }
            <div className={(permissions.create || permissions.edit ? "col-9" : "col-12") + " card card-primary"}>
                <div className="card-body">
                    <FullCalendar ref={calendar}
                        {...calendarProps}
                    />
                </div>
            </div>
            <Modal
                id="ProdDeploy"
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={deployDetails.showDetails}
                onClose={handleCloseModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={deployDetails.showDetails}>
                    <Box sx={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: '75%',
                        bgcolor: 'background.paper',
                        border: '2px solid #000',
                        boxShadow: 24,
                        p: 4,
                    }}>
                        <div className="card">
                            <div className="card-header">
                                <h3 className="row card-title w-100 align-items-center" style={
                                    {
                                        marginBottom: editActive || props.user.role !== "devops" ? "0" : "-15px"
                                    }
                                }>
                                    <div className="col-6">
                                        {editActive ?
                                            <div>
                                                <dt>Título de unidad de negocio</dt>
                                                <input
                                                    ref={deployTitleOT}
                                                    type="text"
                                                    className="form-control form-control-border"
                                                    id="title_updated_deploy"
                                                    placeholder="Título de unidad de negocio"
                                                    defaultValue={deployDetails.details.title}
                                                />
                                            </div>
                                            : <span>
                                                <i className="mr-1 fa fa-scroll" /> {deployDetails.details.title}
                                                {GetStatusDeployComponent(deployDetails.details.buildingStatus)}
                                            </span>}
                                    </div>
                                    <div className="col-6">
                                        {GetSelectionStatus()}
                                        {
                                            permissions.edit ?
                                                <LoadingButton
                                                    className={(!editActive ? "mb-4" : "")}
                                                    color="primary"
                                                    onClick={e => checkEdit()}
                                                    loading={editing}
                                                    loadingPosition="start"
                                                    startIcon={!editActive ? <Edit /> : <Save />}
                                                    variant="contained"
                                                >
                                                    {editActive ? "Guardar" : "Editar"}
                                                </LoadingButton> : []
                                        }
                                        {permissions.delete ?
                                            <Button
                                                className={"ml-2 " + (!editActive ? "mb-4" : "")}
                                                color='error'
                                                onClick={e => {
                                                    if (editActive) {
                                                        setEditActive(false)
                                                        setEditing(false);
                                                        ResetValues();
                                                    }
                                                    else {
                                                        DeleteDeploy(deployDetails.details._id)
                                                        setEditing(false);
                                                        ResetValues();
                                                    }
                                                }}
                                                disabled={editing}
                                                variant="contained"
                                                startIcon={!editActive ? <Delete /> : <Cancel />}>
                                                {editActive ? "Cancelar" : "Eliminar"}
                                            </Button>
                                            : []
                                        }
                                        {
                                            !editActive &&
                                                props.user.role &&
                                                (props.user.role.toUpperCase() === "DEVELOPER") &&
                                                deployDetails.details.buildingStatus === 2 ?
                                                <Button
                                                    className="ml-3"
                                                    color='success'
                                                    onClick={e => {
                                                        handleCloseModal();
                                                        Swal.fire({
                                                            title: '¿Estás seguro que quieres liberar el despliegue?',
                                                            text: "Se liberará para generar las ramas correspondientes",
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: 'green',
                                                            cancelButtonColor: '#3085d6',
                                                            confirmButtonText: 'Liberar',
                                                            cancelButtonText: 'Cancelar'
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                updateDeploy(CreateDeployObject("DEPLOYDETAILS", 1), true)
                                                            }
                                                        });
                                                    }}
                                                    disabled={editing}
                                                    variant="contained"
                                                    startIcon={<CheckCircle />}>
                                                    Liberar
                                                </Button>
                                                : []
                                        }
                                        <IconButton className="mb-3 float-right" onClick={handleCloseModal}>
                                            <Close />
                                        </IconButton>
                                    </div>
                                </h3>
                            </div>
                            <div className="card-body row">
                                <dl className="mr-3" style={{ width: "40%" }}>
                                    <div className="row">
                                        <div className="col-7">
                                            <div className="form-group">
                                                <dt>Título de OT</dt>
                                                {
                                                    editActive ?
                                                        <input
                                                            ref={deployUpdatedOtTitle}
                                                            type="text"
                                                            className="form-control form-control-border"
                                                            id="ot_updated_deploy"
                                                            placeholder="Título"
                                                            defaultValue={deployDetails.details.titleOt}
                                                        /> :
                                                        <dd>{deployDetails.details.titleOt ? deployDetails.details.titleOt : "Sin título"}</dd>
                                                }
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <div className="form-group">
                                                <dt>Tarea de Jira</dt>
                                                {
                                                    editActive ?
                                                        <input
                                                            ref={deployUpdatedJira}
                                                            type="text"
                                                            className="form-control form-control-border"
                                                            id="ot_updated_deploy"
                                                            placeholder="Tarea de Jira"
                                                            defaultValue={deployDetails.details.jira}
                                                        /> :
                                                        <dd>{deployDetails.details.jira ? deployDetails.details.jira : "Sin tarea asignada"}</dd>
                                                }
                                            </div>
                                        </div>
                                        <div className="col-7">
                                            <div className="form-group">
                                                <dt>Número de OT</dt>
                                                {
                                                    editActive ?
                                                        <input
                                                            ref={deployUpdatedOt}
                                                            type="number"
                                                            className="form-control form-control-border"
                                                            id="ot_updated_deploy"
                                                            placeholder="Número de OT"
                                                            defaultValue={deployDetails.details.otNumber}
                                                        /> :
                                                        <dd>{deployDetails.details.otNumber ? deployDetails.details.otNumber : "Sin OT asignada"}</dd>
                                                }
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <div className="form-group">
                                                <dt>Fecha de despliegue</dt>
                                                <dd>{deployDetails.details.date ? new Date(deployDetails.details.date).toLocaleDateString('es-mx', { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' }) : "Sin fecha de despligue asignada"}</dd>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <dt>Descripción del cambio explícito</dt>
                                        {
                                            editActive ?
                                                <textarea
                                                    ref={deployUpdatedDescription}
                                                    className="form-control"
                                                    rows="3"
                                                    placeholder="Descripción"
                                                    defaultValue={deployDetails.details.description}
                                                />
                                                :
                                                <dd style={
                                                    {
                                                        overflowX: 'hidden',
                                                        overflowY: 'auto',
                                                        maxHeight: '250px'
                                                    }
                                                }
                                                >{deployDetails.details.description ? deployDetails.details.description : "Sin descripción"}</dd>
                                        }
                                    </div>
                                </dl>
                                <Divider orientation="vertical" flexItem />
                                <dl style={{ width: "20%" }} className="ml-3 mr-3">
                                    <div className="form-group">
                                        <dt>Link de OT (Jazz)</dt>
                                        <dd>{deployDetails.details.link ? <a href={deployDetails.details.link} target="_BLANK" rel="noopener noreferrer">Detalles</a> : "No se ha registrado link de OT"}</dd>
                                    </div>
                                    <div className="form-group">
                                        {!editActive ? <dt>Responsable DevOps</dt> : []}
                                        {
                                            !editActive ?
                                                <dd>{deployDetails.details.devops ? deployDetails.details.devops.name + " " + deployDetails.details.devops.lastName : "Responsable no asignado"}</dd>
                                                :
                                                <Autocomplete
                                                    className="mb-3"
                                                    options={users.devops}
                                                    autoComplete
                                                    getOptionLabel={(option) => option.name + " " + option.lastName}
                                                    noOptionsText="Sin coincidencias"
                                                    id="open-on-focus-devops"
                                                    size="small"
                                                    openOnFocus
                                                    renderInput={(params) => (
                                                        <TextField {...params} label="Responsable DevOps" variant="standard" />
                                                    )}
                                                    value={selectedUsers.devops}
                                                    onChange={(_event, newDevOpsUser) => {
                                                        setSelectedUsers({
                                                            devops: newDevOpsUser,
                                                            developer1: selectedUsers.developer1,
                                                            developer2: selectedUsers.developer2,
                                                            team: selectedUsers.team
                                                        })
                                                    }}
                                                />
                                        }
                                        {!editActive ? <dt>Desarrollador (Noche)</dt> : []}
                                        {
                                            !editActive ?
                                                <dd>{deployDetails.details.developer1 ? deployDetails.details.developer1 : "Responsable no asignado"}</dd>
                                                :
                                                <input
                                                    type="text"
                                                    className="form-control form-control-border"
                                                    placeholder="Desarrollador (Noche)"
                                                    onChange={e=>{
                                                        setDeployDetails(prevState => {
                                                            let deployDet = Object.assign({}, deployDetails);
                                                            deployDet.details.developer1 = e.target.value
                                                            return deployDet;
                                                        })
                                                    }}
                                                    value={deployDetails.details.developer1}
                                                />
                                            /*
                                            <Autocomplete
                                                className="mb-3"
                                                autoComplete
                                                open={users.developer.length > 0}
                                                options={users.developer}
                                                getOptionLabel={(option) => option.name + " " + option.lastName}
                                                inputValue={inputValue}
                                                noOptionsText="Sin coincidencias"
                                                onInputChange={GetDevelopers}
                                                disableClearable
                                                id="open-on-focus-developer"
                                                size="small"
                                                renderInput={(params) => (
                                                    <TextField {...params} label="Responsable Desarrollador" variant="standard" />
                                                )}
                                                value={selectedUsers.developer}
                                                onChange={(_event, newDeveloperUser) => {
                                                    setSelectedUsers({
                                                        devops: selectedUsers.devops,
                                                        developer: newDeveloperUser,
                                                        team: selectedUsers.team
                                                    })
                                                }}
                                            />*/
                                        }
                                        {!editActive ? <dt>Desarrollador (Mañana)</dt> : []}
                                        {
                                            !editActive ?
                                                <dd>{deployDetails.details.developer2 ? deployDetails.details.developer2 : "Responsable no asignado"}</dd>
                                                :
                                                <input
                                                    type="text"
                                                    className="form-control form-control-border mt-2"
                                                    placeholder="Desarrollador (Mañana)"
                                                    value={deployDetails.details.developer2}
                                                    onChange={e=>{
                                                        setDeployDetails(prevState => {
                                                            let deployDet = Object.assign({}, deployDetails);
                                                            deployDet.details.developer2 = e.target.value
                                                            return deployDet;
                                                        })
                                                    }}
                                                />
                                            /*
                                            <Autocomplete
                                                className="mb-3"
                                                autoComplete
                                                open={users.developer.length > 0}
                                                options={users.developer}
                                                getOptionLabel={(option) => option.name + " " + option.lastName}
                                                inputValue={inputValue}
                                                noOptionsText="Sin coincidencias"
                                                onInputChange={GetDevelopers}
                                                disableClearable
                                                id="open-on-focus-developer"
                                                size="small"
                                                renderInput={(params) => (
                                                    <TextField {...params} label="Responsable Desarrollador" variant="standard" />
                                                )}
                                                value={selectedUsers.developer}
                                                onChange={(_event, newDeveloperUser) => {
                                                    setSelectedUsers({
                                                        devops: selectedUsers.devops,
                                                        developer: newDeveloperUser,
                                                        team: selectedUsers.team
                                                    })
                                                }}
                                            />*/
                                        }
                                    </div>
                                    {editActive ? [] : <dt>Equipo:</dt>}
                                    {
                                        editActive ?
                                            <Autocomplete
                                                className="mb-3"
                                                autoComplete
                                                noOptionsText="Sin coincidencias"
                                                options={Settings.GetTeams()}
                                                getOptionLabel={(option) => option.name}
                                                id="open-on-focus-developer"
                                                size="small"
                                                disableClearable
                                                renderInput={(params) => (
                                                    <TextField {...params} label="Equipo" variant="standard" />
                                                )}
                                                value={selectedUsers.team}
                                                onChange={(_event, newTeam) => {
                                                    setSelectedUsers({
                                                        devops: selectedUsers.devops,
                                                        developer1: selectedUsers.developer1,
                                                        developer2: selectedUsers.developer2,
                                                        team: newTeam
                                                    })
                                                }}
                                            />
                                            :
                                            <dd>{deployDetails.details.team ? GetTeamIcon() : "Equipo no asignado"}</dd>
                                    }
                                </dl>
                                <Divider orientation="vertical" flexItem />
                                <ExecutionSpec User={props.user} DeployDetails={deployDetails.details} Permissions={permissions} />
                            </div>
                        </div>

                    </Box>
                </Fade>
            </Modal>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={ShowOrderExecution}
                onClose={handleCloseShowingOrderExecution}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={ShowOrderExecution}>
                    <Box sx={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: '90%',
                        bgcolor: 'background.paper',
                        border: '2px solid #000',
                        boxShadow: 24,
                        p: 4,
                    }}>
                        <div className="card">
                            <div className="card-header">
                                <h3 className="row card-title w-100 align-items-center">
                                    <div className="col-6">
                                        Orden de ejecución
                                    </div>
                                    <div className="col-6">
                                        <IconButton className="mb-3 float-right" onClick={handleCloseShowingOrderExecution}>
                                            <Close />
                                        </IconButton>
                                    </div>
                                </h3>
                            </div>
                            <div className="card-body">
                                <OrderExecution />
                            </div>
                        </div>
                    </Box>
                </Fade>
            </Modal>
        </div >
    )
}
export default ProdDeployComponent;