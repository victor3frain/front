import React from 'react';
import { Link } from 'react-router-dom';

function Page404() {
    return (
        <div>
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <img src="/media/logos/logo_coe_apis.png" alt="logo_coe_apis" />
                        </div>
                    </div>
                </div>
            </section>
            <section className="content">
                <div className="error-page">
                    <h2 className="headline text-warning"> 404</h2>
                    <div className="error-content">
                        <h3><i className="fas fa-exclamation-triangle text-warning" /> Oops! Página no encontrada.</h3>
                        <p>
                            No pudimos encontrar la página que ests buscando.
                            De cualquier manera, puedes <Link to="/home">regresar al inicio</Link> o intentar usar el buscador.
                        </p>
                        <form className="search-form">
                            <div className="input-group">
                                <input type="text" name="search" className="form-control" placeholder="Search" />
                                <div className="input-group-append">
                                    <button type="submit" name="submit" className="btn btn-warning"><i className="fas fa-search" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    );
}
export default Page404;